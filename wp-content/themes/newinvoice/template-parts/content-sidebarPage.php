<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<div class="col-md-4">
  <div class="headline"><h2>Video</h2></div>
  <?php
      $i = 0;
      if( have_rows('video', 754) ):
          while ( have_rows('video', 754) ) : the_row(); $i++;

      $tieude_video = get_sub_field('tieu_dề_video'); 
      $attachment_id = get_sub_field('ảnh_dại_diện_video'); 
      $link_video = get_sub_field('link_video');

      $size = "rect_thumb"; // (thumbnail, medium, large, full or custom size)
      $image = wp_get_attachment_image_src( $attachment_id, $size );
      $thumbnail = $image[0]; 
  ?>
  <div class="margin-bottom-30 row">
      <div class="col-sm-12">
          <a href="<?php echo $link_video; ?>" class="video-right hidden-sm hidden-xs" rel="zoomphoto">
              <img src="<?php echo $thumbnail; ?>" class="img-responsive lazy" alt="<?php echo $tieude_video; ?>" />
              <span class="play"></span>
              <div class="title"><?php echo $tieude_video; ?></div>
          </a>
          <a href="<?php echo $link_video; ?>" target="_blank" class="video-right hidden-lg hidden-md">
              <img src="<?php echo $thumbnail; ?>" class="img-responsive lazy" alt="<?php echo $tieude_video; ?>" />
              <span class="play"></span>
              <div class="title"><?php echo $tieude_video; ?></div>
          </a>
      </div>
  </div>
  <?php  
      endwhile;
  endif;
  ?>  
  <?php 
    $blog = get_field("blog", 754);
    if($blog){
        $cat_link = get_category_link( $blog );
        $cat_name = get_cat_name( $blog );
    }
    $num_post = get_field("số_bai_viết", 754);
  ?>
  <div class="headline"><h2><a href="<?php echo $cat_link; ?>"><?php echo $cat_name; ?></a></h2></div>
  <ul class="morecnt">
      <?php $i=0; 
          $args = array(
                'post_type'   => 'post',
                'orderby'     => 'date',
                'showposts'   => $num_post,
                'cat'         => (int) $blog
              ); 
      $my_query = new WP_Query( $args );?>
      <?php if ( $my_query->have_posts() ):?>
      <?php while ( $my_query->have_posts() ) : $my_query->the_post(); $i++; 
            // $thumb_post= wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'rect_thumb');
            // $url_thumbnail  = $thumb_post['0'];
      ?>
      <li><a href="<?php echo get_permalink(); ?>"><p><?php echo get_the_title(); ?></p> <span><?php echo get_the_date(); ?></span></a></li>
      <?php endwhile; ?>
      <?php endif; ?> <?php wp_reset_query();?>
  </ul>
</div>