<?php
/**
 * Template part for displaying home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<!--=== Slider ===-->
<!-- <div id="Slider" class="carousel slide carousel-v1 box-shadow shadow-effect-2">
    <ol class="carousel-indicators">
        <li data-slide-to="0" data-target="#Slider" class="rounded-x active"></li>
        <li data-slide-to="1" data-target="#Slider" class="rounded-x"></li>
        <li data-slide-to="2" data-target="#Slider" class="rounded-x"></li>
        <li data-slide-to="3" data-target="#Slider" class="rounded-x"></li>
        <li data-slide-to="4" data-target="#Slider" class="rounded-x"></li>
        <li data-slide-to="5" data-target="#Slider" class="rounded-x"></li>
        <li data-slide-to="6" data-target="#Slider" class="rounded-x"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img alt="hỗ trợ in hóa đơn cho doanh nghiệp nhiều chi nhánh, nhiều cửa hàng" src="<?php bloginfo('template_url');?>/assets/images/ho-tro-in-hoa-don.jpg" class="img-responsive lazy">
        </div>
        <div class="item">
            <img alt="đơn vị hang đầu cung cấp giải pháp hóa đơn điện tử" src="<?php bloginfo('template_url');?>/assets/images/logo-khach-hang.jpg" class="img-responsive lazy">
        </div>
        <div class="item">
            <img alt="Mô hình xuất hóa đơn điện tử xác thực EINVOICE" src="<?php bloginfo('template_url');?>/assets/images/so-do-giai-phap-hoa-don-dien-tu-xac-thuc.png" class="img-responsive lazy">
        </div>
        <div class="item">
            <img alt="xác thực hóa đơn trực tuyến, chống làm giả hóa đơn" src="<?php bloginfo('template_url');?>/assets/images/xac-thuc-hoa-don-truc-tuyen.jpg" class="img-responsive lazy">
        </div>
        <div class="item">
            <img alt="được tổng cục thuế thẩm định" src="<?php bloginfo('template_url');?>/assets/images/tong-cuc-thue-tham-dinh.jpg" class="img-responsive lazy">
        </div>
        <div class="item">
            <img alt="hỗ trợ chuyên nghiệp" src="<?php bloginfo('template_url');?>/assets/images/ho-tro-chuyen-nghiep.jpg" class="img-responsive lazy">
        </div>
        <div class="item">
            <img alt="doanh nghiệp có thể tự thiết kế hóa đơn" src="<?php bloginfo('template_url');?>/assets/images/tu-thiet-ke-hoa-don.jpg" class="img-responsive lazy">
        </div>
    </div>
    <div class="carousel-arrow">
        <a class="left carousel-control" href="#Slider" data-slide="prev">
            <i class="fa-chevron-circle-left fa"></i>
        </a>
        <a class="right carousel-control" href="#Slider" data-slide="next">
            <i class="fa-chevron-circle-right fa"></i>
        </a>
    </div>
</div> -->
<?php 
    $slider = get_field("shortcode_slider");
    echo do_shortcode($slider); 
?>
<div class="purchase">
    <div class="container">
        <div class="row">
            <div class="col-md-6 animated fadeInLeft text-justify">
                <!-- <h1>Phần mềm ho&aacute; đơn điện tử x&aacute;c thực E-INVOICE</h1>

                <p>Phần mềm h&oacute;a đơn điện tử x&aacute;c thực E-Invoice được x&acirc;y dựng dựa tr&ecirc;n <a href="download/Quyet-dinh-1209-2015-Q%c4%90-BTC.doc" target="_blank"><strong>Quyết định 1209/QĐ-BTC của Bộ T&agrave;i Ch&iacute;nh</strong></a> v&agrave; <a href="download/Quyet-dinh-1445-2015-Q%c4%90-TCT.doc" target="_blank"><strong>Quyết định 1445/QĐ-TCT của Tổng cục Thuế</strong></a> về việc sử dụng h&oacute;a đơn điện tử c&oacute; m&atilde; x&aacute;c thực.</p>

                <p>&nbsp;</p>

                <p>Việc sử dụng phần mềm h&oacute;a đơn điện tử x&aacute;c thực&nbsp;E-Invoice mang lại rất nhiều lợi &iacute;ch thiết thực cho doanh nghiệp như: giảm chi ph&iacute; in ấn, lưu trữ v&agrave; vận chuyển h&oacute;a đơn. H&oacute;a đơn được lập tr&ecirc;n m&aacute;y t&iacute;nh, k&yacute; số sau đ&oacute; gửi l&ecirc;n cơ quan thuế để x&aacute;c thực, cơ quan thuế trả về th&ocirc;ng tin h&oacute;a đơn điện tử c&oacute; m&atilde; x&aacute;c thực cho doanh nghiệp. Sau khi h&oacute;a đơn được cơ quan thuế x&aacute;c thực doanh nghiệp thực hiện chuyển h&oacute;a đơn điện tử đến kh&aacute;ch h&agrave;ng bằng phương tiện điện tử như: internet, email, SMS..</p> -->

                <?php echo get_field("nội_dung_block_giới_thiệu"); ?>

                <div class="text-right">
                    <?php
                        $i = 0;
                        if( have_rows('quản_ly_button_lien_kết') ):
                            while ( have_rows('quản_ly_button_lien_kết') ) : the_row(); $i++;

                        $ten_nut_lien_ket = get_sub_field('ten_nut_lien_kết'); 
                        $link_nut_lien_ket = get_sub_field('link_nut_lien_kết'); 
                        $icon_fontawsome = get_sub_field('icon_fontawsome'); 
                    ?>
                    <a class="btn-u btn-u-lg" target="_blank" href="<?php echo $link_nut_lien_ket; ?>"><i class="<?php echo $icon_fontawsome; ?>"></i> <?php echo $ten_nut_lien_ket; ?></a>
                    <?php  
                        endwhile;
                    endif;
                    ?>  
                    <!-- <a class="btn-u btn-u-lg btn2" target="_blank" href="tinh-nang-phan-mem-hoa-don-dien-tu-einvoice.html"><i class="fa fa-cogs"></i> Tính năng</a>
                    <a class="btn-u btn-u-lg btn2" target="_blank" href="lien-he.html"><i class="fa fa-edit"></i> Báo giá</a> -->
                </div>
            </div>
            <div class="col-md-6 animated fadeInRight">
               <div class="margin-bottom-40">
                    <div class="title-box-v2">
                        <h2>Đăng ký sử dụng phần mềm <span class="color-green">NEW</span>INVOICE</h2>
                    </div>
                    <div class="text-justify form-dangky-sudung">
                        <!-- <form action="https://einvoice.vn/lien-he" id="formgr" method="post">                        
                            <div class="form-horizontal padding-top-20">
                                <div class="form-group">
                                    <label for="Name" class="control-label col-md-4">Mã số thuế: <span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" autofocus data-bv-notEmpty="true" name="MST" placeholder="Nhập mã số thuế" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="TEN_DOANH_NGHIEP" class="control-label col-md-4">Tên doanh nghiệp: </label>
                                    <div class="col-md-6">
                                        <input type="text" name="TEN_DOANH_NGHIEP" placeholder="Nhập tên doanh nghiệp" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="SO_DIEN_THOAI" class="control-label col-md-4">Số điện thoại: </label>
                                    <div class="col-md-6">
                                        <input type="text" name="SO_DIEN_THOAI" placeholder="Nhập số điện thoại" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="Content" class="control-label col-md-4">Yêu cầu khác: <span class="red">*</span></label>
                                    <div class="col-md-8">
                                        <textarea name="Content" data-bv-notEmpty="true" cols="8" placeholder="Nhập nội dung liên hệ" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">&nbsp;</label>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary btn-color">Đăng ký</button>
                                        <button type="reset" class="btn btn-default">Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form> -->
                        <?php 
                            $form_dang_ky = get_field("shortcode_form_dang_ky_tu_vấn");
                            echo do_shortcode($form_dang_ky);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Purchase Block -->

<!--=== Content Part ===-->
<div class="container content">
    <!-- Service Blocks -->
    <div class="row margin-bottom-30">
        <?php
            $i = 0;
            if( have_rows('quản_ly_block_uu_diểm') ):
                while ( have_rows('quản_ly_block_uu_diểm') ) : the_row(); $i++;

            $tieu_de_uu_diem = get_sub_field('tieu_dề'); 
            $noi_dung_uu_diem = get_sub_field('nội_dung');
            $link_uu_diem = get_sub_field('link_chi_tiết');
        ?>
        <div class="col-md-3">
            <div class="service">
                <div class="fa fa-check-square-o service-icon">&nbsp;</div>

                <div class="desc">
                    <h4><a href="<?php echo $link_uu_diem; ?>"><?php echo $tieu_de_uu_diem; ?></a></h4>
                </div>

                <p style="text-align:justify"><?php echo $noi_dung_uu_diem; ?></p>

            </div>
        </div>
        <?php  
            endwhile;
        endif;
        ?>  
        <!-- <div class="col-md-3">
            <div class="service">
                <div class="fa fa-check-square-o service-icon">&nbsp;</div>

                <div class="desc">
                    <h4><a href="tinh-nang-phan-mem-hoa-don-dien-tu-einvoice.html">Quản l&yacute; h&oacute;a đơn</a></h4>
                </div>

                <p style="text-align:justify">Phần mềm h&oacute;a đơn điện tử Einvoice gi&uacute;p quản l&yacute;, b&aacute;o c&aacute;o đầy đủ về h&oacute;a đơn, kh&aacute;ch h&agrave;ng, doanh thu, xuất dữ liệu h&oacute;a đơn ra phần mềm k&ecirc; khai thuế</p>

            </div>
        </div>
        <div class="col-md-3">
            <div class="service">
                <div class="fa fa-check-square-o service-icon">&nbsp;</div>

                <div class="desc">
                    <h4><a href="tinh-nang-phan-mem-hoa-don-dien-tu-einvoice.html" target="_blank">Tiết kiệm chi ph&iacute;</a></h4>
                </div>

                <p style="text-align:justify">Phần mềm h&oacute;a đơn điện tử x&aacute;c thực Einvoice gi&uacute;p doanh nghiệp&nbsp;kh&ocirc;ng phải in ấn h&oacute;a đơn, h&oacute;a đơn được lập, vận chuyển h&oacute;a đơn đến kh&aacute;ch h&agrave;ng bằng phương tiện điện tử<br />
                (kh&ocirc;ng phải in, kh&ocirc;ng phải chuyển ph&aacute;t nhanh...)</p>

            </div>
        </div>
        <div class="col-md-3">
            <div class="service">
                <div class="fa fa-check-square-o service-icon">&nbsp;</div>

                <div class="desc">
                    <h4><a href="tinh-nang-phan-mem-hoa-don-dien-tu-einvoice.html">Dễ d&agrave;ng t&iacute;ch hợp</a></h4>
                </div>

                <p style="text-align:justify">Phần mềm h&oacute;a đơn điện tử x&aacute;c thực Einvoice c&oacute; khả năng&nbsp;t&iacute;ch hợp truyền nhận dữ liệu từ c&aacute;c hệ thống phần mềm CRM, ERP, phần mềm kế to&aacute;n v&agrave; c&aacute;c phần mềm c&oacute; sẵn tại doanh nghiệp</p>

            </div>
        </div> -->
    </div>
    <!-- End Service Blokcs -->

    <!-- Recent Works -->
    <?php 
        $cate_blog_1 = get_field("van_bản_hoa_don_diện_tử");
        if($cate_blog_1){
            $cat_link_blog_1 = get_category_link( $cate_blog_1 );
            $cat_name_blog_1 = get_cat_name( $cate_blog_1 );
        }
    ?>
    <div class="headline">
        <a href="<?php echo $cat_link_blog_1; ?>" target="_blank">
            <h2><?php echo $cat_name_blog_1; ?></h2>
            <span class="view">Xem tất cả <i class="fa fa-arrow-circle-right"></i></span>
        </a>
    </div>
    <div class="row margin-bottom-20">
        <?php $i=0; 
              $args = array(
                    'post_type'   => 'post',
                    'orderby'     => 'date',
                    'showposts'   => 4,
                    'cat'         => (int) $cate_blog_1
                  ); 
          $my_query = new WP_Query( $args );?>
          <?php if ( $my_query->have_posts() ):?>
          <?php while ( $my_query->have_posts() ) : $my_query->the_post(); $i++; 
                $thumb_post= wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'rect_thumb');
                $url_thumbnail  = $thumb_post['0'];
          ?>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="<?php echo get_the_title(); ?>" src="<?php echo $url_thumbnail; ?>">
                    </div>
                    <a href="<?php echo get_permalink(); ?>" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="<?php echo get_permalink(); ?>" class="hover-effect"><?php echo get_the_title(); ?></a></h3>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?> <?php wp_reset_query();?>
        <!-- <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="Cục Thuế TP. H&#224; Nội t&#237;ch cực đẩy mạnh triển khai h&#243;a đơn điện tử" src="<?php bloginfo('template_url');?>/assets/images/cnt-Cuc-Thue-TP-Ha-Noi-tich-cuc-day-manh-trien-khai-hoa-don-dien-tu0.jpg">
                    </div>
                    <a href="tin-tuc/cuc-thue-tp-ha-noi-tich-cuc-day-manh-trien-khai-hoa-don-dien-tu.html" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="tin-tuc/cuc-thue-tp-ha-noi-tich-cuc-day-manh-trien-khai-hoa-don-dien-tu.html" class="hover-effect">Cục Thuế TP. H&#224; Nội t&#237;ch cực đẩy mạnh triển khai h&#243;a đơn điện tử</a></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="H&#243;a đơn điện tử c&#243; m&#227; của cơ quan thuế d&#224;nh cho c&#225;c đối tượng n&#224;o?" src="<?php bloginfo('template_url');?>/assets/images/cnt-Hoa-don-dien-tu-co-ma-cua-co-quan-thue-danh-cho-cac-doi-tuong-nao0.jpg">
                    </div>
                    <a href="tin-tuc/hoa-don-dien-tu-co-ma-cua-co-quan-thue-danh-cho-cac-doi-tuong-nao.html" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="tin-tuc/hoa-don-dien-tu-co-ma-cua-co-quan-thue-danh-cho-cac-doi-tuong-nao.html" class="hover-effect">H&#243;a đơn điện tử c&#243; m&#227; của cơ quan thuế d&#224;nh cho c&#225;c đối tượng n&#224;o?</a></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="Th&#244;ng b&#225;o ph&#225;t h&#224;nh h&#243;a đơn qua mạng lần đầu cần ch&#250; &#253; điều g&#236;?" src="<?php bloginfo('template_url');?>/assets/images/cnt-Thong-bao-phat-hanh-hoa-don-qua-mang-lan-dau-can-chu-y-dieu-gi0.png">
                    </div>
                    <a href="tin-tuc/thong-bao-phat-hanh-hoa-don-qua-mang-lan-dau-can-chu-y-dieu-gi.html" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="tin-tuc/thong-bao-phat-hanh-hoa-don-qua-mang-lan-dau-can-chu-y-dieu-gi.html" class="hover-effect">Th&#244;ng b&#225;o ph&#225;t h&#224;nh h&#243;a đơn qua mạng lần đầu cần ch&#250; &#253; điều g&#236;?</a></h3>
                </div>
            </div>
        </div> -->
    </div>
    <!-- End Recent Works -->
    <?php 
        $cate_blog_2 = get_field("tin_tức_hoa_don_diện_tử");
        if($cate_blog_2){
            $cat_link_blog_2 = get_category_link( $cate_blog_2 );
            $cat_name_blog_2 = get_cat_name( $cate_blog_2 );
        }
    ?>
    <!-- Recent Works -->
    <div class="headline">
        <a href="<?php echo $cat_link_blog_2; ?>" target="_blank"><h2><?php echo $cat_name_blog_2; ?></h2><span class="view">Xem tất cả <i class="fa fa-arrow-circle-right"></i></span></a>
    </div>
    <div class="row margin-bottom-20">
        <?php $i=0; 
              $args = array(
                    'post_type'   => 'post',
                    'orderby'     => 'date',
                    'showposts'   => 4,
                    'cat'         => (int) $cate_blog_2
                  ); 
          $my_query = new WP_Query( $args );?>
          <?php if ( $my_query->have_posts() ):?>
          <?php while ( $my_query->have_posts() ) : $my_query->the_post(); $i++; 
                $thumb_post= wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'rect_thumb');
                $url_thumbnail  = $thumb_post['0'];
          ?>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="<?php echo get_the_title(); ?>" src="<?php echo $url_thumbnail; ?>">
                    </div>
                    <a href="<?php echo get_permalink(); ?>" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="<?php echo get_permalink(); ?>" class="hover-effect"><?php echo get_the_title(); ?></a></h3>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?> <?php wp_reset_query();?>
        <!-- <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="Hướng dẫn xuất h&#243;a đơn từ danh s&#225;ch chưa lập tr&#234;n phần mềm h&#243;a đơn điện tử E-Invoice" src="<?php bloginfo('template_url');?>/assets/images/Huong-dan-xuat-hoa-don-tu-danh-sach-chua-lap-tren-phan-mem-hoa-don-dien-tu-E-Invoice12409.png">
                    </div>
                    <a href="tin-tuc/huong-dan-xuat-hoa-don-tu-danh-sach-chua-lap-tren-phan-mem-hoa-don-dien-tu-e-invoice.html" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="tin-tuc/huong-dan-xuat-hoa-don-tu-danh-sach-chua-lap-tren-phan-mem-hoa-don-dien-tu-e-invoice.html" class="hover-effect">Hướng dẫn xuất h&#243;a đơn từ danh s&#225;ch chưa lập tr&#234;n phần mềm h&#243;a đơn điện tử E-Invoice</a></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="Quy định về đăng k&#253; sử dụng h&#243;a đơn điện tử c&#243; m&#227; của cơ quan thuế theo dự thảo Th&#244;ng tư 119" src="<?php bloginfo('template_url');?>/assets/images/cnt-Quy-dinh-ve-dang-ky-su-dung-hoa-don-dien-tu-co-ma-cua-co-quan-thue-theo-du-thao-Thong-tu-1190.jpg">
                    </div>
                    <a href="tin-tuc/quy-dinh-ve-dang-ky-su-dung-hoa-don-dien-tu-co-ma-cua-co-quan-thue-theo-du-thao-thong-tu-119.html" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="tin-tuc/quy-dinh-ve-dang-ky-su-dung-hoa-don-dien-tu-co-ma-cua-co-quan-thue-theo-du-thao-thong-tu-119.html" class="hover-effect">Quy định về đăng k&#253; sử dụng h&#243;a đơn điện tử c&#243; m&#227; của cơ quan thuế theo dự thảo Th&#244;ng tư 119</a></h3>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="thumbnails thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="imggr">
                        <img alt="Những trường hợp bắt buộc ngừng sử dụng h&#243;a đơn điện tử c&#243; m&#227; của cơ quan thuế" src="<?php bloginfo('template_url');?>/assets/images/Nhung-truong-hop-bat-buoc-ngung-su-dung-hoa-don-dien-tu-co-ma-cua-co-quan-thue12406.jpg">
                    </div>
                    <a href="tin-tuc/nhung-truong-hop-bat-buoc-ngung-su-dung-hoa-don-dien-tu-co-ma-cua-co-quan-thue.html" class="btn-more hover-effect">chi tiết +</a>
                </div>
                <div class="caption">
                    <h3><a href="tin-tuc/nhung-truong-hop-bat-buoc-ngung-su-dung-hoa-don-dien-tu-co-ma-cua-co-quan-thue.html" class="hover-effect">Những trường hợp bắt buộc ngừng sử dụng h&#243;a đơn điện tử c&#243; m&#227; của cơ quan thuế</a></h3>
                </div>
            </div>
        </div> -->
    </div>
    <!-- End Recent Works -->

    <!-- Info Blokcs -->
    <div class="row margin-bottom-30">
        <!-- Welcome Block -->
        <div class="col-md-8 md-margin-bottom-40">
            <div class="headline"><a href="<?php echo get_field("link_giới_thiệu"); ?>" target="_blank"><h2><?php echo get_field("ten_cong_ty"); ?></h2></a></div>
            <div class="row">
                <div class="col-sm-4">
                    <img src="<?php echo get_field("ảnh_mo_tả"); ?>" alt="NewInvoice" class="img-responsive margin-bottom-20 lazy" />

                </div>
                <div class="col-sm-8">
                    <div class="list-unstyled margin-bottom-20">
                        <!-- <ul>
                           <li>
                               <div class="fa fa-check color-green">&nbsp;</div>
                               Phục vụ tr&ecirc;n <strong>50 ngh&igrave;n</strong> doanh nghiệp khai Thuế v&agrave; Hải quan
                           </li>
                            <li>
                               <div class="fa fa-check color-green">&nbsp;</div>
                               L&agrave; đơn vị cung cấp dịch vụ khai thuế <strong>TVAN</strong> cho doanh nghiệp
                           </li>
                           <li>
                               <div class="fa fa-check color-green">&nbsp;</div>
                               3 năm li&ecirc;n tiếp đạt giải thưởng sao khu&ecirc; trong lĩnh vực Thuế v&agrave; Hải quan <strong>(2009,2010,2011)</strong>
                           </li>
                           <li>
                               <div class="fa fa-check color-green">&nbsp;</div>
                               Tr&ecirc;n 10 năm kinh nghiệm đồng h&agrave;nh c&ugrave;ng doanh nghiệp trong lĩnh vực <strong>Thuế</strong> v&agrave; <strong>Hải quan</strong>
                           </li>
                           <li>
                               <div class="fa fa-check color-green">&nbsp;</div>
                               Hỗ trợ doanh nghiệp 24/7 - <strong>Tin Cậy, Tận T&igrave;nh, Chuy&ecirc;n Nghiệp</strong>
                           </li>
                       </ul> -->
                       <?php echo get_field("giới_thiệu_cong_ty"); ?>
                   </div>
               </div>
           </div>
       </div><!--/col-md-8-->

       <!-- Latest Shots -->
       <div class="col-md-4">
            <div class="headline">
                <a href="<?php echo get_field("link_giới_thiệu"); ?>" target="_blank"><h2>Hình ảnh công ty</h2></a>
            </div>
            <div class="carousel slide carousel-v1" id="myCarousel">
                <div class="carousel-inner">
                    <?php
                        $i = 0;
                        if( have_rows('quản_ly_hinh_ảnh_giới_thiệu_cong_ty') ):
                            while ( have_rows('quản_ly_hinh_ảnh_giới_thiệu_cong_ty') ) : the_row(); $i++;

                        $hinhanh_cty = get_sub_field('hinh_ảnh_giới_thiệu_cong_ty'); 
                        $text_cty = get_sub_field('mo_tả_ảnh');

                        $size = "rect_thumb"; // (thumbnail, medium, large, full or custom size)
                        $image = wp_get_attachment_image_src( $hinhanh_cty, $size );
                        $thumbnail = $image[0];  

                        $size_full = "full"; // (thumbnail, medium, large, full or custom size)
                        $image_full = wp_get_attachment_image_src( $hinhanh_cty, $size_full );
                        $thumbnail_full = $image_full[0];
                    ?>
                    <div class="item <?php if($i==1){ echo 'active'; }else{ echo ''; } ?>">
                        <a class="zoom-photo" rel="zoomphoto" href="<?php echo $thumbnail_full; ?>"><img alt="<?php echo $text_cty; ?>" class="lazy" src="<?php echo $thumbnail; ?>"></a>
                        <div class="carousel-caption">
                            <p><?php echo $text_cty; ?></p>
                        </div>
                    </div>
                    <?php  
                        endwhile;
                    endif;
                    ?>   
                    <!-- <div class="item ">
                        <a class="zoom-photo" rel="zoomphoto" href="assets/images/sub-Doi-ngu-nhan-vien6.jpg"><img alt="Đội ngũ hỗ trợ nhiệt t&#236;nh" class="lazy" src="<?php bloginfo('template_url');?>/assets/images/Doi-ngu-nhan-vien6.jpg"></a>
                        <div class="carousel-caption">
                            <p>Đội ngũ hỗ trợ nhiệt t&#236;nh</p>
                        </div>
                    </div>
                    <div class="item ">
                        <a class="zoom-photo" rel="zoomphoto" href="assets/images/sub-Doi-ngu-nhan-vien7.jpg"><img alt="Đội ngũ hỗ trợ nhiệt t&#236;nh" class="lazy" src="<?php bloginfo('template_url');?>/assets/images/Doi-ngu-nhan-vien7.jpg"></a>
                        <div class="carousel-caption">
                            <p>Đội ngũ hỗ trợ nhiệt t&#236;nh</p>
                        </div>
                    </div>
                    <div class="item ">
                        <a class="zoom-photo" rel="zoomphoto" href="assets/images/sub-Nhan-vien-cong-ty0.jpg"><img alt="Đội ngũ hỗ trợ nhiệt t&#236;nh" class="lazy" src="<?php bloginfo('template_url');?>/assets/images/Nhan-vien-cong-ty0.jpg"></a>
                        <div class="carousel-caption">
                            <p>Đội ngũ hỗ trợ nhiệt t&#236;nh</p>
                        </div>
                    </div> -->
                </div>
                <div class="carousel-arrow">
                    <a data-slide="prev" href="#myCarousel" class="left carousel-control">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a data-slide="next" href="#myCarousel" class="right carousel-control">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div><!--/col-md-4-->
    </div>
    <!-- End Info Blokcs -->

    <?php get_template_part('template-parts/content','customers'); ?>
</div><!--/container-->
<!-- End Content Part -->