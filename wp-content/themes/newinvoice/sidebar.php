<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newinvoice
 */
global $data;
?>
<!-- SIDEBAR-RIGHT -->
<div class="col-sm-3 side-bar">
    <div class="headline"><h3>Tin tức nổi bật</h3></div>
    <ul class="list-news-thumb">
        <?php $i=0; 
          $args = array(
                'post_type'   => 'post',
                'orderby'     => 'date',
                'showposts'   => 5,
                'cat'         => 1
              ); 
        $my_query = new WP_Query( $args );?>
        <?php if ( $my_query->have_posts() ):?>
        <?php while ( $my_query->have_posts() ) : $my_query->the_post(); $i++; 
            $thumb_post= wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'rect_thumb');
            $url_thumbnail  = $thumb_post['0'];
        ?>
        <li class="clearfix">
            <div class="img-thumb">
                <img src="<?php echo $url_thumbnail; ?>" alt="<?php echo get_the_title(); ?>">
            </div>
            <div class="info-thumb">
                <h5><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>
            </div>
        </li>
        <?php endwhile; ?>
        <?php endif; ?> <?php wp_reset_query();?>
    </ul>
    <div class="scroller_anchor">
    </div>
    <div class="suggest_block lpdr hidden-xs">
        <div class="headline"><h3>Có thể bạn quan tâm</h3></div>
        <?php if(function_exists('wp_nav_menu')){wp_nav_menu( 'theme_location=menu-sidebar&menu_class=u1&container=""');} ?>
        <ul class="u2"><li class="l1"><?php echo $data['hotline1']; ?></li><li class="l2">(Hãy gọi ngay cho NewInvoice)</li></ul>
        <a style="cursor: pointer;" class="send-request" data-toggle="modal" data-target="#send_request">
            Gửi yêu cầu
        </a>
    </div>
</div>
<!-- END SIDEBAR-RIGHT -->
