<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newinvoice
 */
global $data;
?>
<!--=== Footer Version 1 ===-->
        <footer class="opportunitie-footer" id="bizapp-footer">
            <div class="container">
                <div class="l_top">
                    <div class="col-xs-12 col-sm-3 col-md-4">
                        <!-- <li class="e-heading" style="margin-left: 0;"> 
                            YÊU CẦU TƯ VẤN
                        </li> -->
                        <!-- <form id="inquiry_form_id" role="form" class="inquiry_form">
                            <div class="contact-title">Chúng tôi sẵn sàng phục vụ, vui lòng điền đầy đủ thông tin bên dưới đây:</div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Điện Thoại:</label>
                                <input type="text" class="form-control" id="phone" name="phone">
                            </div>
                            <div class="form-group" style="margin-bottom: 10px">
                                <label for="message">Lời Nhắn:</label>
                                <input type="text" class="form-control" id="message" name="message">
                            </div>
                            <button type="submit" class="btn btn-primary submit">GỬI YÊU CẦU</button>
                        </form> -->
                        <?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 1')) : else : ?><?php endif; ?>
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2">
                        <!-- <ul>
                            <li class="e-heading"> Trợ Giúp</li>
                            <div class="li-block">
                                <li><a href="/forum/tro-giup-1">Hỏi đáp</a></li>
                                <li><a href="/page/huong-dan/">Tài liệu hướng dẫn</a></li>
                                <li><a href="/page/videos/">Videos</a></li>
                                <li><a href="/blog">Chia sẻ</a></li>
                            </div>
                            <li class="e-heading">Về chúng tôi</li>
                            <div class="li-block">
                                <li><a href="/page/gioi-thieu">Giới thiệu</a></li>
                                <li><a href="/page/bang-gia">Bảng giá</a></li>
                                <li><a href="/khach-hang-cua-chung-toi">Khách hàng</a></li>
                                <li><a href="/page/tuyen-dung">Tuyển dụng</a></li>
                                <li><a href="/page/contactus">Liên hệ</a></li>
                            </div>
                        </ul> -->
                        <?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 2')) : else : ?><?php endif; ?>
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2">
                        <!-- <ul>
                            <li class="e-heading">ĐỐI TÁC</li>
                            <div class="li-block">
                                <li><a href="/page/hop-tac/">Lời mời hợp tác</a></li>
                                <li><a href="/tim-doi-tac/">Tìm đối tác</a></li>
                            </div>
                            <li class="e-heading">PHIÊN BẢN MOBILE</li>
                            <div class="li-block">
                                <li><a href="https://play.google.com/store/apps/details?id=com.odoo.mobile&amp;hl=vi">Android</a></li>
                                <li><a href="https://itunes.apple.com/us/app/odoo/id1272543640?mt=8">IOS</a></li>
                            </div>
                        </ul> -->
                        <?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 3')) : else : ?><?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 bg_footer">
                        <ul>
                            <li class="e-heading">
                                <font class="text-white" style="">Liên Hệ</font>
                            </li>
                          <li>
                            <div class="info-line-left">
                              <i class="fa fa-map-marker text-gray-lighter" aria-hidden="true" style=""></i>
                            </div>
                            <div class="info-line-right">
                              <font class="text-gray-lighter">Văn phòng chính</font>
                            </div>
                          </li>
                          <li>
                            <div class="info-line-left">
                                </div>
                            <div class="info-line-right">
                              <font class="text-gray-lighter"><?php echo $data['diachi']; ?></font>
                            </div>
                          </li>
                          <li>
                            <hr>
                          </li>
                          <li>
                            <div class="info-line-left">
                              <i class="fa fa-phone text-gray-lighter" aria-hidden="true" style=""></i>
                            </div>
                            <div class="info-line-right">
                              <font class="text-gray-lighter" style="">Tổng đài hỗ trợ:&nbsp;</font>
                              <font class="text-gray-lighter" style=""><?php echo $data['tongdai']; ?></font>
                            </div>
                          </li>
                          <li>
                            <div class="info-line-left">
                              <i class="fa fa-globe text-gray-lighter" aria-hidden="true" style=""></i>
                            </div>
                            <div class="info-line-right">
                              <font class="text-gray-lighter" style="">Email:&nbsp;</font>
                              <a class="active" href="mailto:<?php echo $data['email']; ?>">
                                <font class="text-gray" style="color:#2c85dd"><?php echo $data['email']; ?></font>
                              </a>
                            </div>
                          </li>
                          <li>
                            <div class="info-line-left">
                              <i class="fa fa-tablet text-gray-lighter" aria-hidden="true" style=""></i>
                            </div>
                            <div class="info-line-right">
                              <font class="text-gray-lighter" style="">Số hotline ngoài giờ: <?php echo $data['hotline2']; ?></font>
                            </div>
                          </li>
                          <li>
                            <div class="info-line-left">
                                </div>
                            <div class="info-line-right">
                              <font class="text-gray-lighter"><?php echo $data['workingtime']; ?></font>
                            </div>
                          </li>
                          <li>
                            <div class="info-line-right" style="display: flex; float: left;">
                              <a href="<?php echo $data['hidden_link_youtube']; ?>" style="padding-right:10px;"><i class="fa fa-youtube text-gray-lighter" aria-hidden="true" style=""></i>
                                                Youtube
                                                </a>
                              <a href="<?php echo $data['hidden_link_fb']; ?>" style="padding-right:10px;"><i class="fa fa-facebook-square text-gray-lighter" aria-hidden="true"></i>
                                                Facebook
                                            </a>
                            </div>
                          </li>
                          <li>
                            <div class="info-line-right" style="display: flex; float: left;">
                              <a href="<?php echo $data['hidden_link_linkedin']; ?>" style="padding-right:10px;"><i class="fa fa-lg fa-linkedin-square text-gray-lighter" aria-hidden="true"></i>
                                                Linkedin
                                            </a>
                              <a href="<?php echo $data['hidden_link_twitter']; ?>" style="padding-right:10px;"><i class="fa fa-lg fa-twitter-square text-gray-lighter" aria-hidden="true"></i>
                                                Twitter
                                            </a>
                            </div>
                          </li>
                        </ul>
                  </div>
                </div>
            </div>
              <div class="container-fluid" id="footer-bottom" style="margin-top:50px;">
                <div class="container">
                  <div class="row">
                    <div class="footer">
                      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 col-left">
                        <?php if(function_exists('wp_nav_menu')){wp_nav_menu( 'theme_location=menu-copyright&menu_class=list-inline pull-left&container=""');} ?>
                      </div>
                    </div>
                  </div>
                </div>
                <i class="gi-gradient-line"></i>
              </div>
        </footer>
        <!--=== End Footer Version 1 ===-->
    </div>

    <div id="share-buttons" class="hidden-xs">
        <!-- Facebook -->
        <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url');?>/assets/images/shareface.png" alt="Facebook" /></a>
        <!-- Twitter -->
        <a href="http://twitter.com/share?url=<?php the_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url');?>/assets/images/sharetweet.png" alt="Twitter" /></a>
        <!-- Google+ -->
        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url');?>/assets/images/sharegoogle.png" alt="Google" /></a>
        <!-- LinkedIn -->
        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>" target="_blank"><img src="<?php bloginfo('template_url');?>/assets/images/sharelinkedin.png" alt="LinkedIn" /></a>
    </div> 

    <script src="<?php bloginfo('template_url');?>/assets/js/respond.js"></script>

    <script src="<?php bloginfo('template_url');?>/assets/js/jquery-migrate.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/back-to-top.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/modernizr.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/jquery.cslider.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/jquery.flexslider-min.js"></script>
    
    <script src="<?php bloginfo('template_url');?>/assets/js/custom.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/parallax-slider.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/jquery.prettyPhoto.js"></script>

    <script src="<?php bloginfo('template_url');?>/assets/js/app.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/site.js"></script>

    
    <?php wp_footer(); ?>
</body>
</html>