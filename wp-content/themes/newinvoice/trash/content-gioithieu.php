<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<div class="breadcrumbs ">
    <div class="wrap">
       <div class="inner"><span><span><a href="https://kenhsango.com/" >Trang chủ</a> <i class="far fa-chevron-right"></i> <span class="breadcrumb_last" aria-current="page">Giới thiệu Website</span></span></span></div>
    </div>
 </div>
 <div class="site-inner">
    <div class="wrap">
       <div class="content-sidebar-wrap">
          <main class="content" id="pure-content">
             <article class="entry post-3268 page type-page status-publish hentry single-content" itemscope itemtype="https://schema.org/CreativeWork">
                <header class="entry-header">
                   <h1 class="entry-title" itemprop="headline">Giới thiệu Website</h1>
                </header>
                <div class="entry-content" itemprop="text">
                   <div data-elementor-type="post" data-elementor-id="3268" class="elementor elementor-3268 elementor-bc-flex-widget" data-elementor-settings="[]">
                      <div class="elementor-inner">
                         <div class="elementor-section-wrap">
                            <section class="elementor-element elementor-element-a206f6d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="a206f6d" data-element_type="section">
                               <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-row">
                                     <div class="elementor-element elementor-element-4e3fa9c elementor-column elementor-col-100 elementor-top-column" data-id="4e3fa9c" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                           <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-38be573 elementor-widget elementor-widget-text-editor" data-id="38be573" data-element_type="widget" data-widget_type="text-editor.default">
                                                 <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                       <p><strong>Kênh sàn gỗ</strong> là hệ thống bán lẻ vật liệu sàn gỗ, sàn nhựa; giấy dán tường, vật liệu hoàn thiện tại kho được thành lập năm 2008 đến nay đã có uy tín hơn 10 năm trên thị trường vât liệu xây dựng nói chung.</p>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </section>
                            <section class="elementor-element elementor-element-7e83127 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="7e83127" data-element_type="section">
                               <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-row">
                                     <div class="elementor-element elementor-element-b2842a9 elementor-column elementor-col-33 elementor-top-column" data-id="b2842a9" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                           <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-c70a72a elementor-widget elementor-widget-text-editor" data-id="c70a72a" data-element_type="widget" data-widget_type="text-editor.default">
                                                 <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                       <p style="text-align: center;"><strong>Cam kết vàng</strong></p>
                                                       <p>&#8211; Tư vấn miễn phí toàn bộ các vấn đề về thiết kế ,thi công, vật liệu, chất lượng, giá cả ,tiến độ, bảo hành.<br />&#8211; Bán đúng sản phẩm nguồn gốc xuất xứ</p>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                     <div class="elementor-element elementor-element-6999694 elementor-column elementor-col-33 elementor-top-column" data-id="6999694" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                           <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-b58a9d3 elementor-widget elementor-widget-text-editor" data-id="b58a9d3" data-element_type="widget" data-widget_type="text-editor.default">
                                                 <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                       <p style="text-align: center;"><span style="font-size: 12pt;"><strong>Vì Sao chọn chúng tôi</strong></span></p>
                                                       <p>&#8211; Tư vấn thiết kế thi công chuyên nghiệp, đội ngũ lắp đặt có nhiều năm kinh nghiệm tận tâm<br />&#8211; Giá tốt, bảo hành, bảo trì lâu dài, chỉ cần khách hàng gọi điện đến chúng tôi Hotline 0981 705 705<br />&#8211; Để đảm bảo uy tín và độ tin cậy cho khách hàng chúng tôi có kho chứa hàng tại ngõ 7 Tôn Thất Thuyết và Showroom 301 Hoàng Quốc Việt &#8211; Cầu Giấy &#8211; Hà Nội</p>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                     <div class="elementor-element elementor-element-441032e elementor-column elementor-col-33 elementor-top-column" data-id="441032e" data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                           <div class="elementor-widget-wrap">
                                              <div class="elementor-element elementor-element-64d25dc elementor-widget elementor-widget-text-editor" data-id="64d25dc" data-element_type="widget" data-widget_type="text-editor.default">
                                                 <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                       <p style="text-align: center;"><strong>Công ty TNHH và đầu tư Vĩnh Gia &#8211; KenhSanGo.com</strong></p>
                                                       <p>&#8211; Được thành lập từ năm 2008 đến nay đã có 10 năm uy tín trên thị trường sàn gỗ, vật liệu xây dựng.<br />&#8211; Ngày 20 tháng 6 năm 2008 từ một cửa hàng nhỏ chúng tôi mới ra mắt, được khách hàng quan tâm và nhu cầu thị trường sàn gỗ phát triển đến năm 2015 công ty TNHH đầu tư Vĩnh Gia ra đời. </p>
                                                       <p>&#8211; Chuyên nhập khẩu và phân phối sàn gỗ , vật liệu xây dựng với mục tiêu hướng tới những sản phẩm mang thương hiệu Đức, Malaysia, Thái Lan&#8230;. với chất lượng cao mẫu mã đa dạng đáp ứng thị hiếu người tiêu dùng trong nước</p>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </section>
                         </div>
                      </div>
                   </div>
                </div>
                <footer class="entry-footer"></footer>
             </article>
          </main>
          <aside class="sidebar sidebar-primary widget-area" role="complementary" aria-label="Sidebar chính" itemscope itemtype="https://schema.org/WPSideBar">
             <section id="categories-2" class="widget widget_categories">
                <div class="widget-inner">
                   <h3 class="widget-title"><span>Chuyên mục</span></h3>
                   <ul>
                      <li class="cat-item cat-item-25"><a href="https://kenhsango.com/do-dung/" >Đồ dùng</a></li>
                      <li class="cat-item cat-item-23"><a href="https://kenhsango.com/nha-dat/" >Nhà đất</a></li>
                      <li class="cat-item cat-item-22"><a href="https://kenhsango.com/phong-thuy/" >Phong thủy</a></li>
                      <li class="cat-item cat-item-1"><a href="https://kenhsango.com/tin-tuc/" >Tin tức</a></li>
                      <li class="cat-item cat-item-24"><a href="https://kenhsango.com/trang-tri/" >Trang trí</a></li>
                   </ul>
                </div>
             </section>
             <section id="tintuc_widget-2" class="widget widget_tintuc_widget">
                <div class="widget-inner">
                   <h3 class="widget-title"><span>Tin tức</span></h3>
                   <div class="owl-carousel owl-widget owl-theme">
                      <div class="item">
                         <a href="https://kenhsango.com/bao-gia-thi-cong-san-go-nam-2018/"><img width="2048" height="1531" src="https://kenhsango.com/wp-content/uploads/2018/10/io-h02-2172-m8234-master.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://kenhsango.com/wp-content/uploads/2018/10/io-h02-2172-m8234-master.jpg 2048w, https://kenhsango.com/wp-content/uploads/2018/10/io-h02-2172-m8234-master-600x449.jpg 600w, https://kenhsango.com/wp-content/uploads/2018/10/io-h02-2172-m8234-master-768x574.jpg 768w, https://kenhsango.com/wp-content/uploads/2018/10/io-h02-2172-m8234-master-1024x766.jpg 1024w" sizes="(max-width: 2048px) 100vw, 2048px" /></a>
                         <h4><a href="https://kenhsango.com/bao-gia-thi-cong-san-go-nam-2018/">Báo giá thi công sàn gỗ năm 2018</a></h4>
                      </div>
                      <div class="item">
                         <a href="https://kenhsango.com/noi-that-chung-cu-van-phong-biet-thu-dep/"><img width="2048" height="1170" src="https://kenhsango.com/wp-content/uploads/2018/09/z1118923977925-26de75e1fb167ab863a05c3fa640cd5b.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://kenhsango.com/wp-content/uploads/2018/09/z1118923977925-26de75e1fb167ab863a05c3fa640cd5b.jpg 2048w, https://kenhsango.com/wp-content/uploads/2018/09/z1118923977925-26de75e1fb167ab863a05c3fa640cd5b-600x343.jpg 600w, https://kenhsango.com/wp-content/uploads/2018/09/z1118923977925-26de75e1fb167ab863a05c3fa640cd5b-768x439.jpg 768w, https://kenhsango.com/wp-content/uploads/2018/09/z1118923977925-26de75e1fb167ab863a05c3fa640cd5b-1024x585.jpg 1024w" sizes="(max-width: 2048px) 100vw, 2048px" /></a>
                         <h4><a href="https://kenhsango.com/noi-that-chung-cu-van-phong-biet-thu-dep/">Nội thất chung cư, văn phòng, biệt thự hiện đại, đẳng cấp</a></h4>
                      </div>
                      <div class="item">
                         <a href="https://kenhsango.com/mau-san-go-cong-nghiep-chiu-nuoc-tot-nhat-2018/"><img width="700" height="484" src="https://kenhsango.com/wp-content/uploads/2018/08/a11-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://kenhsango.com/wp-content/uploads/2018/08/a11-1.jpg 700w, https://kenhsango.com/wp-content/uploads/2018/08/a11-1-600x415.jpg 600w" sizes="(max-width: 700px) 100vw, 700px" /></a>
                         <h4><a href="https://kenhsango.com/mau-san-go-cong-nghiep-chiu-nuoc-tot-nhat-2018/">Mẫu sàn gỗ công nghiệp chịu nước tốt nhất 2018</a></h4>
                      </div>
                      <div class="item">
                         <a href="https://kenhsango.com/lam-nha-tan-co-dien-thi-dung-san-go-mau-nao/"><img width="960" height="719" src="https://kenhsango.com/wp-content/uploads/2018/09/41779161-671840753184262-2857161684391297024-n.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://kenhsango.com/wp-content/uploads/2018/09/41779161-671840753184262-2857161684391297024-n.jpg 960w, https://kenhsango.com/wp-content/uploads/2018/09/41779161-671840753184262-2857161684391297024-n-600x449.jpg 600w, https://kenhsango.com/wp-content/uploads/2018/09/41779161-671840753184262-2857161684391297024-n-768x575.jpg 768w" sizes="(max-width: 960px) 100vw, 960px" /></a>
                         <h4><a href="https://kenhsango.com/lam-nha-tan-co-dien-thi-dung-san-go-mau-nao/">Làm nhà Tân Cổ Điển thì dùng sàn gỗ màu nào ?</a></h4>
                      </div>
                   </div>
                </div>
             </section>
          </aside>
       </div>
    </div>
 </div>
 <div class="sidebar-bottom">
    <div class="wrap"></div>
 </div>