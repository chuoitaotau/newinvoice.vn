<?php
/**
 * The template for displaying breadcrumbs
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hanko
 */	
?>
<div class="breadcrumbs">
    <div class="container wrap-bread">
        <?php 
	        $category = get_the_category();
			$firstCategory = $category[0]->cat_name;
        ?>
        <h1 class="pull-left"><?php echo $firstCategory; ?></h1>
        <?php if(function_exists('bcn_display'))
	    {
	        bcn_display();
	    }?>
    </div>
</div>