<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<!-- PageMainContent -->
<?php get_template_part('template-parts/content','breadcrumb'); ?>
<div class="container content">
    <div class="margin-bottom-40">
        <div class="title-box-v2">
            <?php echo get_field("tieu_dề"); ?>
        </div>
        <div class="text-justify">


            <div class="row">
                <div class="col-md-8 form-lien-he">
                    <?php 
                    $shortcode_form_lien_he = get_field("shortcode_form_lien_hệ");
                    echo do_shortcode($shortcode_form_lien_he);
                    ?>
                </div>
                <div class="col-md-4">
                    <div class="branch margin-bottom-40">
                        <?php
                            $i = 0;
                            if( have_rows('quản_ly_thong_tin_lien_hệ') ):
                                while ( have_rows('quản_ly_thong_tin_lien_hệ') ) : the_row(); $i++;

                            $chi_nhanh = get_sub_field('chi_nhanh'); 
                            $dia_chi = get_sub_field('dịa_chỉ'); 
                            $sdt  = get_sub_field('số_diện_thoại'); 
                        ?>
                        <div class="item">
                            <b><i class="fa fa-building"></i> <?php echo $chi_nhanh; ?></b><br />
                            <i class="fa fa-circle"></i> <?php echo $dia_chi; ?><br />
                            <i class="fa fa-circle"></i> <?php echo $sdt; ?>
                        </div>
                        <?php  
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php 
                echo get_field("dịa_chỉ_google_maps");
            ?>
        </div>
    </div>


    <!-- Our Clients -->
    <?php get_template_part('template-parts/content','customers'); ?>
    <!-- //End Our Clients -->
</div>
 <!--=== End PageMainContent ===-->