<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<div class="partner-page">
    <!-- Banner -->
    <div class="partner-banner">
        <div class="container">
            <div class="new-col-md-24 no-padding partner-banner-text-box">
                <div class="new-col-md-24 no-padding partner-banner-content">
                    <div class="partner-banner-content-right">
                        <img src="<?php bloginfo('template_url');?>/assets/images/icon-partner.svg" alt="newinvoice.vn" />
                    </div>
                    <div class="partner-banner-content-left">
                        <div class="partner-banner-content-left-content">
                            <div class="partner-banner-title">
                                <?php echo get_field("tieu_dề_1_block_1"); ?>
                            </div>
                            <div class="partner-banner-title">
                                <?php echo get_field("tieu_dề_2_block_1"); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="new-col-md-24 no-padding partner-banner-subtitle">
                    <?php echo get_field("mo_tả_ngắn_block_1"); ?>
                </div>
                <div class="new-col-md-24 no-padding partner-banner-horizontal-line"></div>
                <!-- <div class="new-col-md-24 no-padding partner-banner-qr-code">
                    <div class="partner-banner-qr-code-image-right">
                        <img src="<?php bloginfo('template_url');?>/assets/images/icon-qr-code.svg" alt="newinvoice.vn" />
                    </div>
                    <div class="partner-banner-qr-code-image-left">
                        <div class="partner-banner-qr-code-images">
                            <a href="https://play.google.com/store/apps/details?id=vn.com.misa.affiliate" target="_blank">
                                <img class="partner-banner-qr-code-image-first" src="<?php bloginfo('template_url');?>/assets/images/img-white-google-play.svg" alt="newinvoice" />
                            </a>
                            <a href="https://itunes.apple.com/app/id1456340971" target="_blank">
                                <img class="partner-banner-qr-code-image-second" src="<?php bloginfo('template_url');?>/assets/images/img-white-apple-store.svg" alt="newinvoice" />
                            </a>
                        </div>
                        <div class="partner-banner-qr-code-text">Quét mã QR code để cài đặt ứng dụng và đăng ký hợp tác</div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!---->
    <!--Giới thiệu-->
    <div class="partner-introduce">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24 partner-introduce-box">
                    <div class="new-col-md-12 no-padding partner-introduce-image-box">
                        <img class="img-responsive" src="<?php echo get_field("ảnh_mo_tả_block_2"); ?>" alt="newinvoice" />
                    </div>
                    <div class="new-col-md-12 no-padding">
                        <div class="partner-introduce-text-box padding-left-10-percent">
                            <h2>
                                <div class="partner-introduce-title"><?php echo get_field("tieu_dề_block_2"); ?></div>
                                <div class="partner-introduce-name"><?php echo get_field("mo_tả_ngắn_block_2"); ?></div>
                            </h2>
                            <div class="partner-introduce-detail">
                                <?php echo get_field("nội_dung_block_2"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- 3 lý do -->
    <div class="partner-reason">
        <div class="container">
            <div class="row">
                <h2>
                    <div class="partner-reason-title"><?php echo get_field("tieu_dề_chung_block_3"); ?></div>
                </h2>
                <div class="new-col-md-24 no-padding partner-reason-content">
                    <div class="new-col-md-24 no-padding partner-reason-item">
                        <div class="new-col-md-12 image-item">
                            <img class="img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_3"); ?>" alt="newinvoice.vn" />
                        </div>
                        <div class="new-col-md-12 text-item">
                            <div class="title-item">
                                <!-- <div class="number-title-item">
                                    01.
                                </div>
                                <div class="subtitle-content-item">
                                    <div class="subtitle-item">
                                        newinvoice.vn-
                                    </div>
                                    <div class="subtitle-item">
                                        Phần mềm hóa đơn điện tử tốt nhất
                                    </div>
                                    <div class="horizontal-line-item"></div>
                                </div> -->
                                <?php echo get_field("tieu_dề_block_3"); ?>
                            </div>
                            <!-- <div class="check-item">
                                <div class="check-content-item">
                                    Đáp ứng đầy đủ nghiệp vụ theo Nghị định 119/2018/NĐ-CP, Thông tư 32/2011/TT-BTC và Thông tư 39/2014/TT-BTC
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Khởi tạo và phát hành hóa đơn điện tử mọi lúc, mọi nơi qua mobile, website, desktop tránh gián đoạn công tác bán hàng
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Sản phẩm duy nhất ứng dụng công nghệ Blockchain giúp an toàn, bảo mật tuyệt đối
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Có hơn 20.000 doanh nghiệp sử dụng trong một năm ra mắt
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item last-child">
                                    Được nhận định là giải pháp hóa đơn điện tử hàng đầu tại Việt Nam trên VnExpress, Dân trí, Tạp chí thuế,...
                                </div>
                            </div>
                            <div class="button-item">
                                <a href="tinh-nang.html" class="read-more-button">Xem chi tiết</a>
                            </div> -->
                            <?php echo get_field("nội_dung_block_3"); ?>
                        </div>
                    </div>
                    <div class="new-col-md-24 no-padding partner-reason-item">
                        <div class="new-col-md-12 image-item">
                            <img class="img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_4"); ?>" alt="newinvoice.vn" />
                        </div>
                        <div class="new-col-md-12 text-item">
                            <!-- <div class="title-item">
                                <div class="number-title-item">
                                    02.
                                </div>
                                <div class="subtitle-content-item">
                                    <div class="subtitle-item">
                                        MISA-
                                    </div>
                                    <div class="subtitle-item">
                                        Công ty uy tín 25 năm kinh nghiệm
                                    </div>
                                    <div class="horizontal-line-item"></div>
                                </div>
                            </div> -->
                            <?php echo get_field("tieu_dề_block_4"); ?>
                            <!-- <div class="check-item">
                                <div class="check-content-item">
                                    Thương hiệu uy tín với 25 năm được nhiều khách hàng tin tưởng, lựa chọn
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Hơn 200.000 tổ chức và hàng triệu cá nhân đang sử dụng sản phẩm
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Đạt giải thưởng SAO KHUÊ, Giải pháp công nghệ thông tin ưa chuộng, Huy chương Vàng ICT Việt Nam, Cúp ICT Awards,...
                                </div>
                            </div> -->
                            <?php echo get_field("nội_dung_block_4"); ?>
                        </div>
                    </div>
                    <div class="new-col-md-24 no-padding partner-reason-item">
                        <div class="new-col-md-12 image-item">
                            <img class="img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_5"); ?>" alt="newinvoice.vn" />
                        </div>
                        <div class="new-col-md-12 text-item">
                            <div class="title-item">
                                <!-- <div class="number-title-item">
                                    03.
                                </div>
                                <div class="subtitle-content-item">
                                    <div class="subtitle-item">
                                        Chính sách hợp tác tốt,
                                    </div>
                                    <div class="subtitle-item">
                                        chiết khấu cao
                                    </div>
                                    <div class="horizontal-line-item"></div>
                                </div> -->
                                <?php echo get_field("tieu_dề_block_5"); ?>
                            </div>
                            <!-- <div class="check-item">
                                <div class="check-content-item">
                                    Được hưởng tỷ lệ chiết khấu hấp dẫn trên tổng giá trị đơn hàng
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Được đào tạo bài bản kiến thức về sản phẩm và khách hàng
                                </div>
                            </div>
                            <div class="check-item">
                                <div class="check-content-item">
                                    Được MISA hỗ trợ cách tư vấn hướng dẫn cho khách hàng đăng ký và sử dụng phần mềm
                                </div>
                            </div> -->
                            <?php echo get_field("nội_dung_block_5"); ?>
                        </div>
                    </div>
                </div>
                <div class="new-col-md-24 partner-reason-button-box">
                    <a src="<?php echo get_field("link_giới_thiệu"); ?>" class="partner-reason-demo-button play-video">Phim giới thiệu</a>
                    <a target="_blank" href="<?php echo get_field("tim_hiểu_them"); ?>" class="partner-reason-read-more-button">Tìm hiểu thêm</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Cộng tác viên, đại lý -->
    <div class="partner-agency-collaborator">
        <div class="partner-agency-collaborator-content">
            <div role="tabpanel">
                <ul class="nav nav-tabs main-tabstript partner-agency-collaborator-tabs">
                    <li class="partner-agency-li active">
                        <a id="agency" href="#tab1" data-toggle="tab"><?php echo get_field("tieu_dề_block_6"); ?></a>
                    </li>
                    <li class="partner-collaborator-li">
                        <a id="collaborator" href="#tab2" data-toggle="tab"><?php echo get_field("tieu_dề_block_7"); ?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab1" class="tab-pane active">
                        <div class="new-col-md-24 partner-agency-collaborator-title"><?php echo get_field("mo_tả_ngắn_block_6"); ?></div>
                        <div class="new-col-md-24 no-padding partner-agency-content">
                            <div class="container">
                                <div class="row">
                                    <div class="new-col-md-12 new-col-md-offset-12">
                                        <div class="partner-agency-collaborator-text-box padding-left-10-percent">
                                            <!-- <div class="partner-agency-text-box">
                                                
                                                <div class="partner-agency-discount-text">
                                                    Chiết khấu cực cao 
                                                </div>
                                            </div>
                                            <div class="partner-agency-horizontal-line"></div>
                                            <div class="partner-agency-title">
                                                Giới thiệu sản phẩm hóa đơn điện tử newinvoice.vn của MISA, khách hàng đặt mua thành công là bạn nhận chiết khấu
                                            </div>
                                            <div class="partner-agency-subtitle">
                                                Mọi đối tượng đều có thể đăng ký làm cộng tác viên
                                            </div>
                                            <div class="partner-agency-read-more">
                                                
                                                Tìm hiểu thêm và Đăng ký <a href="https://offer.newinvoice.vn/cong_tac_vien_misa?utm_source=Hop-tac" target="_blank">TẠI ĐÂY</a>
                                            </div> -->
                                            <?php echo get_field("nội_dung_block_6"); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab2" class="tab-pane">
                        <div class="new-col-md-24 partner-agency-collaborator-title"><?php echo get_field("mo_tả_ngắn_block_7"); ?></div>
                        <div class="new-col-md-24 no-padding partner-collaborator-content">
                            <div class="container">
                                <div class="row">
                                    <div class="new-col-md-12">
                                        <div class="partner-agency-collaborator-text-box padding-right-10-percent">
                                            <!-- <div class="partner-collaborator-text-box">
                                                
                                                <div class="partner-collaborator-discount-text">
                                                    Chiết khấu cực cao 
                                                </div>
                                            </div>
                                            <div class="partner-collaborator-horizontal-line"></div>
                                            <div class="partner-collaborator-title">
                                                Khi tìm kiếm và chào bán sản phẩm cho khách hàng
                                            </div>
                                            <div class="partner-collaborator-subtitle">
                                                Đối tượng đủ điều kiện đăng ký: đại lý thuế, công ty dịch vụ kế toán, công ty kiểm toán, công ty/trung tâm đào tạo kế toán...
                                            </div>
                                            <div class="partner-collaborator-read-more">
                                                
                                                Tìm hiểu thêm và Đăng ký <a href="https://offer.newinvoice.vn/cong_tac_vien_misa?utm_source=Hop-tac" target="_blank">TẠI ĐÂY</a>
                                            </div> -->
                                            <?php echo get_field("nội_dung_block_7"); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- Nhanh tay đăng ký -->
    <div class="partner-register-title-box">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24">
                    <h2>
                        <div class="partner-register-title"><?php echo get_field("tieu_dề_block_8"); ?></div>
                        <div class="partner-register-name"><?php echo get_field("mo_tả_ngắn_block_8"); ?></div>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="partner-register">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24">
                    <div class="new-col-md-10 no-padding partner-register-box-right">
                        <!-- <form id="formDangkyDoanhnghiep">
                            <h4><strong>DOANH NGHIỆP</strong></h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="dnmasothue" placeholder="Mã số thuế (*)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="dnhovaten" placeholder="Người liên hệ (*)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="dnsodienthoai" placeholder="Số điện thoại (*)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="dnemail" placeholder="Email (*)">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning">ĐĂNG KÝ NGAY</button>
                            </div>
                        </form> -->
                        <?php 
                            $doanh_nghiep = get_field("shortcode_form_dang_ky_dại_ly_danh_cho_doanh_nghiệp");
                            echo do_shortcode($doanh_nghiep); 
                        ?>
                    </div>

                    <div class="new-col-md-10 no-padding partner-register-box-left" style="margin-top: 50px; margin-left: 60px">
                        <!-- <form id="formDangkyCanhan">
                            <h4><strong>THÔNG TIN CÁ NHÂN</strong></h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cnhovaten" placeholder="Họ và tên (*)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cndiachi" placeholder="Địa chỉ (*)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cndienthoai" placeholder="Số điện thoại (*)">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cnemail" placeholder="Email (*)">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning">ĐĂNG KÝ NGAY</button>
                            </div>
                        </form> -->
                        <?php 
                            $ca_nhan = get_field("shortcode_form_dang_ky_dại_ly_danh_cho_ca_nhan");
                            echo do_shortcode($ca_nhan); 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
</div>