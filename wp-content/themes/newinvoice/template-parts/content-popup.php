<?php
/**
 * The template for displaying breadcrumbs
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hanko
 */	
?>
<script type="text/javascript">
    $(window).scroll(function(e){var scroller_anchor = $(".scroller_anchor").offset().top;if ($(this).scrollTop() >= scroller_anchor && $('.suggest_block').css('position') != 'fixed'){$('.suggest_block').css({'position': 'fixed','top': '70px'});}else if ($(this).scrollTop() < scroller_anchor && $('.suggest_block').css('position') != 'relative'){$('.scroller_anchor').css('height', '0px');$('.suggest_block').css({'position': 'relative'});}});
</script>
<!-- Modal -->
<div class="modal fade" id="send_request" role="dialog">
    <div class="modal-dialog">
          <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông tin của bạn</h4>
            </div>
            <div class="modal-body">
               <!-- <form id="inquiry_form_id" role="form" class="inquiry_form">
                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-md-3 col-form-label">Email:</label>
                        <div class="col-sm-8 col-md-9">
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone"  class="col-sm-4 col-md-3 col-form-label">Điện Thoại (*):</label>
                        <div class="col-sm-8 col-md-9">
                            <input required="" type="text" class="form-control" id="phone" name="phone">
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 10px">
                        <label for="message"  class="col-sm-4 col-md-3 col-form-label">Lời Nhắn:</label>
                        <div class="col-sm-8 col-md-9">
                            <input type="text" class="form-control" id="message" name="message">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary submit">GỬI YÊU CẦU</button>
                </form> -->
                <?php echo do_shortcode("[ninja_form id=5]"); ?>
            </div>
        </div>
    </div>
</div>