<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package newinvoice
 */

get_header();
?>
<?php get_template_part('template-parts/content','breadcrumb'); ?>
<div class="container content">
    <div class="row">
        <div class="col-sm-9">
           <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
           <div class="mb-margin-bottom-30">
            <h1 class="title"><?php echo get_the_title(); ?></h1>
            <div class="list-unstyled list-inline blog-info">
                <i class="fa fa-calendar"></i> <?php echo get_the_date(); ?>
                
                <div class="clearfix"></div>
            </div>
            <div class="textcnt">
                <?php the_content(); ?>
            </div>
           </div>
            <?php endwhile; else: ?>
            <?php echo _e('Nội dung đang được cập nhật...'); ?>
            <?php endif; ?><?php wp_reset_postdata();?>
        </div>

        <?php echo get_sidebar(); ?>
    </div>

    <?php get_template_part('template-parts/content','customers'); ?>
</div>
<?php get_template_part('template-parts/content','popup'); ?>
 <!--=== End PageMainContent ===-->
<?php
get_footer();