<?php

add_action('init', 'of_options');

if (!function_exists('of_options')) {
    function of_options()
    {
        //Access the WordPress Categories via an Array
        $of_categories = array();
        // $of_categories_obj 	= get_categories('hide_empty=0');  Nếu muốn lấy theo cate tin tức thì lấy cái này
        $of_categories_obj = get_categories('taxonomy=product_cat&hide_empty=0&hierarchical=1');
        foreach ($of_categories_obj as $of_cat) {
            $of_categories[$of_cat->cat_ID] = $of_cat->cat_name . ' - ' . $of_cat->cat_ID;
        }
        $categories_tmp = array_unshift($of_categories, "Select a category:");

        //Access the WordPress Pages via an Array
        $of_pages = array();
        $of_pages_obj = get_pages('sort_column=post_parent,menu_order');
        foreach ($of_pages_obj as $of_page) {
            $of_pages[$of_page->ID] = $of_page->post_name;
        }
        $of_pages_tmp = array_unshift($of_pages, "Select a page:");

        //Testing
        $of_options_select = array("one", "two", "three", "four", "five");
        $of_options_radio = array("one" => "One", "two" => "Two", "three" => "Three", "four" => "Four", "five" => "Five");

        //Sample Homepage blocks for the layout manager (sorter)
        $of_options_homepage_blocks = array
        (
            "disabled" => array(
                "placebo" => "placebo", //REQUIRED!
                "block_one" => "Block One",
                "block_two" => "Block Two",
                "block_three" => "Block Three",
            ),
            "enabled" => array(
                "placebo" => "placebo", //REQUIRED!
                "block_four" => "Block Four",
            ),
        );


        //Stylesheets Reader
        $alt_stylesheet_path = LAYOUT_PATH;
        $alt_stylesheets = array();

        if (is_dir($alt_stylesheet_path)) {
            if ($alt_stylesheet_dir = opendir($alt_stylesheet_path)) {
                while (($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false) {
                    if (stristr($alt_stylesheet_file, ".css") !== false) {
                        $alt_stylesheets[] = $alt_stylesheet_file;
                    }
                }
            }
        }


        //Background Images Reader
        $bg_images_path = get_stylesheet_directory() . '/images/bg/'; // change this to where you store your bg images
        $bg_images_url = get_template_directory_uri() . '/images/bg/'; // change this to where you store your bg images
        $bg_images = array();

        if (is_dir($bg_images_path)) {
            if ($bg_images_dir = opendir($bg_images_path)) {
                while (($bg_images_file = readdir($bg_images_dir)) !== false) {
                    if (stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
                        natsort($bg_images); //Sorts the array into a natural order
                        $bg_images[] = $bg_images_url . $bg_images_file;
                    }
                }
            }
        }


        /*-----------------------------------------------------------------------------------*/
        /* TO DO: Add options/functions that use these */
        /*-----------------------------------------------------------------------------------*/

        //More Options
        $uploads_arr = wp_upload_dir();
        $all_uploads_path = $uploads_arr['path'];
        $all_uploads = get_option('of_uploads');
        $other_entries = array("Select a number:", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19");
        $body_repeat = array("no-repeat", "repeat-x", "repeat-y", "repeat");
        $body_pos = array("top left", "top center", "top right", "center left", "center center", "center right", "bottom left", "bottom center", "bottom right");

        // Image Alignment radio box
        $of_options_thumb_align = array("alignleft" => "Left", "alignright" => "Right", "aligncenter" => "Center");

        // Image Links to Options
        $of_options_image_link_to = array("image" => "The Image", "post" => "The Post");


        /*-----------------------------------------------------------------------------------*/
        /* The Options Array */
        /*-----------------------------------------------------------------------------------*/

// Set the Options Array
        global $of_options;
        $of_options = array();


        /*-----------------------------------------------------------------------------------*/
        /* Phần Header */
        /*-----------------------------------------------------------------------------------*/

        $of_options[] = array("name" => "Header",
            "type" => "heading"
        );
        $of_options[] = array("name" => "Hello there!",
            "desc" => "",
            "id" => "introduction",
            "std" => "<h3 style=\"margin: 0 0 10px;\">Phần thiết lập Logo / Favicon</h3>
                        Phần thiết lập logo, favicon...
                    ",
            "icon" => true,
            "type" => "info"
        );
        $of_options[] = array("name" => "Logo Header",
            "desc" => "Upload logo cho website của bạn.",
            "id" => "logo_img",
            // Use the shortcodes [site_url] or [site_url_secure] for setting default URLs
            "std" => "",
            "type" => "upload"
        );
        
        $of_options[] = array("name" => "Favicon Image",
            "desc" => "Upload Favicon cho website của bạn.",
            "id" => "favicon_img",
            // Use the shortcodes [site_url] or [site_url_secure] for setting default URLs
            "std" => "",
            "type" => "upload"
        );

        /*-----------------------------------------------------------------------------------*/
        /* Phần Footer */
        /*-----------------------------------------------------------------------------------*/
        $of_options[] = array("name" => "Footer",
            "type" => "heading"
        );
        $of_options[] = array("name" => "Hello there!",
            "desc" => "",
            "id" => "introduction",
            "std" => "<h3 style=\"margin: 0 0 10px;\">Phần Thiết Lập Footer.</h3>
                        Phần thiết lập logo footer, code google analytic, master tool ...
                    ",
            "icon" => true,
            "type" => "info"
        );
        
        $of_options[] = array("name" => "Google Analytics",
            "desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
            "id" => "google_analytics",
            "std" => "",
            "type" => "textarea"
        );
        $of_options[] = array("name" => "Google Webmaster Tool",
            "desc" => "Paste your Google Webmaster Tool (or other) tracking code here. This will be added into the footer template of your theme.",
            "id" => "google_webmaster_tool",
            "std" => "",
            "type" => "textarea"
        );
        $of_options[] = array("name" => "Remarketing adwords",
            "desc" => "Paste your Remarketing adwords (or other) tracking code here. This will be added into the footer template of your theme.",
            "id" => "remarketing_adwords",
            "std" => "",
            "type" => "textarea"
        );

        /*-----------------------------------------------------------------------------------*/
        /* Phần thiết lập chung */
        /*-----------------------------------------------------------------------------------*/


        $of_options[] = array("name" => "Thiết Lập Chung",
            "type" => "heading",
            "icon" => ADMIN_IMAGES . "icon-settings.png"
        );

        $of_options[] = array("name" => "Hello there!",
            "desc" => "",
            "id" => "introduction",
            "std" => "<h3 style=\"margin: 0 0 10px;\">Phần Thiết Lập Chung.</h3>
						Phần thiết lập số điện thoại hotline, support online qua Skype và Yahoo.
					",
            "icon" => true,
            "type" => "info"
        );

        $of_options[] = array("name" => "Hotline 1",
            "desc" => "Bạn nhập Hotline 1 cho toàn trang",
            "id" => "hotline1",
            "std" => "",
            "type" => "text"
        );

        $of_options[] = array("name" => "Hotline 2",
            "desc" => "Bạn nhập Hotline cho toàn trang",
            "id" => "hotline2",
            "std" => "",
            "type" => "text"
        );

        $of_options[] = array("name" => "Tổng đài hỗ trợ",
            "desc" => "Bạn nhập Tổng đài hỗ trợ cho toàn trang",
            "id" => "tongdai",
            "std" => "",
            "type" => "text"
        );


        $of_options[] = array("name" => "Email",
            "desc" => "Bạn nhập Email cho toàn trang",
            "id" => "email",
            "std" => "",
            "type" => "text"
        );

        $of_options[] = array("name" => "Địa chỉ",
            "desc" => "Bạn nhập Địa chỉ cho toàn trang",
            "id" => "diachi",
            "std" => "",
            "type" => "text"
        );

        $of_options[] = array("name" => "Thời gian làm việc",
            "desc" => "Bạn nhập thời gian làm việc cho toàn trang",
            "id" => "workingtime",
            "std" => "",
            "type" => "text"
        );


        $of_options[] = array("name" => "Copyright",
            "desc" => "Bạn nhập Copyright cho toàn trang",
            "id" => "copyright",
            "std" => "",
            "type" => "text"
        );

        /*-----------------------------------------------------------------------------------*/
        /* Phần Mạng Xã Hội */
        /*-----------------------------------------------------------------------------------*/
        $of_options[] = array("name" => "Tích Hợp Mạng XH",
            "type" => "heading",
            "icon" => ADMIN_IMAGES . "social_network.png"
        );
        $of_options[] = array("name" => "Hello there!",
            "desc" => "",
            "id" => "introduction",
            "std" => "<h3 style=\"margin: 0 0 10px;\">Phần Tích Hợp Mạng XH</h3>
                        Phần thiết lập link liên kết FB, GG, Youtube
                    ",
            "icon" => true,
            "type" => "info"
        );
        $of_options[] = array("name" => "Social Network",
            "desc" => "Tích hợp mạng xã hội.",
            "id" => "offline",
            "std" => 0,
            "folds" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array("name" => "Facebook",
            "desc" => "Bạn điền Link Fanpage",
            "id" => "hidden_link_fb",
            "std" => "",
            "fold" => "offline", /* the checkbox hook */
            "type" => "text"
        );

        $of_options[] = array("name" => "Youtube",
            "desc" => "Bạn điền Link Page YouTube",
            "id" => "hidden_link_youtube",
            "std" => "",
            "fold" => "offline", /* the checkbox hook */
            "type" => "text"
        );

        $of_options[] = array("name" => "Twitter",
            "desc" => "Bạn điền Link Twitter",
            "id" => "hidden_link_twitter",
            "std" => "",
            "fold" => "offline", /* the checkbox hook */
            "type" => "text"
        );

        $of_options[] = array("name" => "Linkedin",
            "desc" => "Bạn điền Link Skype",
            "id" => "hidden_link_linkedin",
            "std" => "",
            "fold" => "offline", /* the checkbox hook */
            "type" => "text"
        );

        // $of_options[] = array("name" => "Skype",
        //     "desc" => "Bạn điền Link Skype",
        //     "id" => "hidden_link_skype",
        //     "std" => "",
        //     "fold" => "offline", /* the checkbox hook */
        //     "type" => "text"
        // );

        /*-----------------------------------------------------------------------------------*/
        /* Backup Options
        /*-----------------------------------------------------------------------------------*/

        $of_options[] = array("name" => "Sao Lưu Cài Đặt",
            "type" => "heading",
            "icon" => ADMIN_IMAGES . "icon-backup.png"
        );

        $of_options[] = array("name" => "Backup and Restore Options",
            "id" => "of_backup",
            "std" => "",
            "type" => "backup",
            "desc" => 'Bạn có thể sử dụng chức năng backup và restore để lựa chọn sao lưu hiện tại của bạn, và sau đó khôi phục nó trở lại cấu hình cài đặt trước. Điều này rất hữu ích nếu bạn muốn thử nghiệm trên các tùy chọn nhưng muốn giữ lại thiết lập cũ trong trường hợp bạn khôi phục trở lại.',
        );

        $of_options[] = array("name" => "Transfer Theme Options Data",
            "id" => "of_transfer",
            "std" => "",
            "type" => "transfer",
            "desc" => 'Bạn có thể truyền dữ liệu tùy chọn lưu giữa cài đặt khác nhau bằng cách sao chép văn bản bên trong hộp văn bản.
					    Để nhập dữ liệu từ một cài đặt, thay thế các dữ liệu trong hộp văn bản với một mã cài đặt khác và bấm vào "Import Option".',
        );

    }//End function: of_options()
}//End check if function exists: of_options()
?>