<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<form method="post" action="<?php the_permalink(); ?>" id="form1" enctype="multipart/form-data">
  <div class="panel-default body-lookup" >
      <div id="content">
          <table style="width: 100%; margin-top: 90px;">
              <tr>
                  <td class="TCHD">Tra cứu Hóa đơn Điện tử
                      <img style="cursor: pointer" id="imgSupport" data-toggle="popover" src="<?php bloginfo('template_url');?>/assets/images/CauChamHoi.png" />
                  </td>
              </tr>
              <tr>
                  <td class="note">Vui lòng nhập mã hóa đơn để tìm kiếm hóa đơn của bạn.<span class="spInfo">
                      &nbsp;<img src="<?php bloginfo('template_url');?>/assets/images/info_note.png" /></span></td>
              </tr>
              <tr>
                  <td align="center">
                      <div class="input-group">
                          <div class="divInvoiceCode">
                              <input name="maHoaDon" type="text" maxlength="20" id="txtInvoiceCode" tabindex="2" class="form-control" placeholder="Nhập Mã tra cứu Hóa đơn" data-toggle="popover" />
                              <img src="<?php bloginfo('template_url');?>/assets/images/SearchXML.png" class="SearchXML" id="imgXML" />
                          </div>
                          <div class="input-group-btn">
                              
                              <span id="button-container">
                                  <input type="submit" name="traCuu" value="Tra cứu" id="btnSearch" class="btn btn-primary btnSearch" />
                              </span>
                          </div>
                      </div>
                  </td>
              </tr>
              <tr>
                <td align="center">
                    <?php
                      //5B669A05
                      if(isset($_POST['maHoaDon'])){
                          $maHoaDon = $_POST['maHoaDon'];

                          $api_url = "http://login.new-invoice.com/update/api/invoices/research/$maHoaDon";
                          // $ch = curl_init();
                          // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                          // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                          $response = wp_remote_get( $api_url, array(  'timeout' => 10,'headers' => array( 'X-Authorization-Token' => 'NewInvoice', 'Content-Type'=> 'application/pdf' )));

                          $name="$maHoaDon.pdf";
                          //var_dump(openssl_get_cert_locations());
                          //var_dump($response); die();
                          $decocedData = wp_remote_retrieve_body( $response );
                          
                          $hasInvoice = json_decode($response['body'])->Message;
                          if($hasInvoice === NULL){
                            $myfile = fopen(get_template_directory()."/invoices/$name","w");
                            fwrite($myfile,$decocedData);
                            fclose($myfile);
                            $a = get_template_directory_uri()."/invoices/$name"; ?>
                            <p>Tìm kiếm thành công - Tải xuống: <a style="color:#ff0000"; href='<?php echo $a; ?>' download><?php echo $name; ?></a></p>
                    <?php
                          }else{
                            echo '<p style="color:#ff0000";>Không tìm thấy hóa đơn</p>';
                          }   
                      }
                    ?>
                </td>
              </tr>
              
          </table>
          
      </div>

      <div class="panel-footer-lookup">
          <footer id="footerTCHD" class="footerTCHD">
              <div style="width: 1003px" class="center">
                  <div class="" style="width: 803px; float: left; display: block;">
                      <div id="headFooter" class="headFooter">
                          <div>
                              <span class="headFooterText"><?php echo get_field("tieu_dề"); ?></span>
                              <br>
                              <span class="headFooterText1"><?php echo get_field("mo_tả_ngắn"); ?></span>
                          </div>
                      </div>
                      <div class="divhr">
                          <hr>
                      </div>
                      <div class="contentFooter">
                          <?php
                              $i = 0;
                              $col = 4;
                              if( have_rows('tieu_chi') ):
                                  while ( have_rows('tieu_chi') ) : the_row(); $i++;

                              $icon_tieu_chi = get_sub_field('icon_tieu_chi'); 
                              $tieude_tieuchi = get_sub_field('tieu_dề_tieu_chi'); 
                              $mota_tieuchi = get_sub_field('mo_tả_ngắn_tieu_chi'); 
                              
                              if($i == 1){ $col = 4; }else if($i == 2){ $col = 5; }else if($i == 3){ $col = 3; }else{ $col = 4; }
                          ?>
                          <div class="col-sm-<?php echo $col; ?>" style="margin-left: -16px;">
                              <img src="<?php echo $icon_tieu_chi; ?>" class="imgstyle imgFooter1">
                              <br>
                              <p class="p1">
                                  <?php echo $tieude_tieuchi; ?>
                              </p>
                              <p class="p2">
                                  <?php echo $mota_tieuchi; ?>
                              </p>
                          </div>
                          <?php  
                              endwhile;
                          endif;
                          ?>
                      </div>

                  </div>
                  <div class="divDK" style="float: left">
                      <p class="p3">
                          <?php echo get_field("dang_ky_ngay"); ?>
                      </p>

                      <input type="button" class="btn btn-primary1" onclick="location.href = '<?php echo get_field("link_dang_ky"); ?>'; target = '_blank'" value="Đăng ký ngay">
                  </div>
              </div>
          </footer>
      </div>