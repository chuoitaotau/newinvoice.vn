<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<!-- PageMainContent -->
<?php get_template_part('template-parts/content','breadcrumb'); ?>

<div class="container content">
    <div class="margin-bottom-40">
        <div class="textcnt">
            <div class="row">
                <div class="col-md-4"><img alt="Phần mềm hóa đơn điện tử xác thực Newinvoice" src="<?php echo get_field("ảnh_mo_tả"); ?>" style="width:60%" /></div>

                <div class="col-md-8">
                    <?php
                        $i = 0;
                        if( have_rows('quản_ly_tải_về_file') ):
                            while ( have_rows('quản_ly_tải_về_file') ) : the_row(); $i++;

                        $icon_taive = get_sub_field('icon_tải_về'); 
                        $noidung_file = get_sub_field('nội_dung_file_tải_về'); 
                    ?>
                    <div class="down-item-list">
                        <img src="<?php echo $icon_taive; ?>" alt="file tải về" />
                        <?php  echo $noidung_file; ?>
                    </div>
                    <?php  
                        endwhile;
                    endif;
                    ?>    
                    
                </div>
           </div>
           <?php
                $i = 0;
                if( have_rows('quản_ly_tải_về_huớng_dẫn') ):
                    while ( have_rows('quản_ly_tải_về_huớng_dẫn') ) : the_row(); $i++;

                $tieu_de_bo_huong_dan = get_sub_field('tieu_dề_bộ_huớng_dẫn'); 
                
            ?>
           <p><strong><?php echo $tieu_de_bo_huong_dan; ?></strong></p>

           <table class="table table-bordered table-hover table-responsive table-striped">
               <thead>
                    <tr>
                         <td style="text-align:center; width:50px"><strong>STT</strong></td>
                         <td><strong>T&agrave;i liệu hướng dẫn</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $j = 0;
                        if( have_rows('tai_liệu_huớng_dẫn') ):
                            while ( have_rows('tai_liệu_huớng_dẫn') ) : the_row(); $j++;

                            $ten_tai_lieu = get_sub_field('ten_tai_liệu'); 
                            $link_tai_lieu = get_sub_field('link_tai_liệu'); 
                            
                    ?>    
                    <tr>
                        <td style="text-align:center"><?php echo $j; ?></td>
                        <td><a href="<?php echo $link_tai_lieu; ?>" target="_blank"><?php echo $ten_tai_lieu; ?></a></td>
                    </tr>
                    <?php  
                        endwhile;
                    endif;
                    ?>   
                </tbody>
            </table>
            <?php  
                endwhile;
            endif;
            ?>    
        </div>
    </div>

    <div class="headline"><h2>Video</h2></div>
        <div class="row margin-bottom-30">
        <?php
            $i = 0;
            if( have_rows('video_huớng_dẫn') ):
                while ( have_rows('video_huớng_dẫn') ) : the_row(); $i++;

            $tieude_video = get_sub_field('tieu_dề_video'); 
            $link_video =  get_sub_field('link_video');
            $attachment_id = get_sub_field('ảnh_mo_tả_video'); 

            $size = "rect_thumb"; // (thumbnail, medium, large, full or custom size)
            $image = wp_get_attachment_image_src( $attachment_id, $size );
            $thumbnail = $image[0];
        ?>
        <div class="col-md-3 col-sm-12">
            <a href="<?php echo $link_video; ?>" class="video-right hidden-sm hidden-xs margin-bottom-10" rel="zoomphoto">
                <img src="<?php echo $thumbnail; ?>" class="img-responsive lazy" alt=" <?php echo $tieude_video; ?>" />
                <span class="play"></span>
                <div class="title"> <?php echo $tieude_video; ?></div>
            </a>
            <a href="<?php echo $link_video; ?>" target="_blank" class="video-right hidden-lg hidden-md margin-bottom-10">
                <img src="<?php echo $thumbnail; ?>" class="img-responsive lazy" alt="<?php echo $tieude_video; ?>" />
                <span class="play"></span>
                <div class="title"> <?php echo $tieude_video; ?></div>
            </a>
        </div>
        <?php  
            endwhile;
        endif;
        ?>
    </div>

    <div class="template margin-bottom-20">
        <div class="headline"><h2>Một số mẫu hóa đơn</h2></div>
        <div class="row">
            <?php
                $h = 0;
                if( have_rows('mỗi_hoa_don') ):
                    while ( have_rows('mỗi_hoa_don') ) : the_row(); $h++;

                $hoadon = get_sub_field('ảnh_mẫu_hoa_don'); 
            ?>
            <div class="col-sm-6 hidden-xs text-center">
                <img id="img-show" class="img-responsive lazy" src="<?php echo $hoadon; ?>" />
            </div>
            <?php  
                if($h==1){ break; }
                endwhile;
            endif;
            ?>
            <div class="col-sm-6 col-xs-12 img-wrapper">
                <?php
                    $k = 0;
                    if( have_rows('mỗi_hoa_don') ):
                    while ( have_rows('mỗi_hoa_don') ) : the_row(); $k++;

                    $hoadon = get_sub_field('ảnh_mẫu_hoa_don'); 
                ?>
                <div class="col-sm-4 col-xs-12 zoomer <?php if($k == 1){ echo "active"; }else{ echo ""; } ?>">
                    <div class="overlay-zoom">
                        <img class="img-responsive lazy" src="<?php echo $hoadon; ?>" />
                        <span class="zoom-icon"></span>
                    </div>
                </div>
                <?php  
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>

    <?php get_template_part('template-parts/content','customers'); ?>
</div>
 <!--=== End PageMainContent ===-->