<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<div class=" mrcms-content">
    <div class="features-page">
        <!-- Banner -->
        <div class="features-banner">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24 banner-box">
                        <div class="new-col-md-12 left-content no-padding">
                            <div class="new-col-md-24 no-padding content-banner">
                                <h1>
                                    <?php echo get_field("tieu_dề_block_1"); ?>
                                </h1>
                                <div class="banner-title">
                                    <?php echo get_field("mo_tả_ngắn_block_1"); ?>
                                </div>
                            </div>
                            <div class="new-col-md-24 no-padding button-features-banner">
                                <div class="button-features">
                                    <div src="<?php echo get_field("link_xem_demo"); ?>" class="new-col-md-12 video-demo play-video">Xem demo</div>
                                    <a href="<?php echo get_field("link_mua_ngay"); ?>" target="_blank" class="new-col-md-12 buy-now">Mua ngay</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Text dưới Banner -->
        <div class="features-text-below-banner">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24">
                        <div class="features-text-below-banner-box">
                            <?php echo get_field("text_kết_nối"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Nghiệp vụ hóa đơn -->
        <div class="features-einvoice-major">
            <div class="container">
                <div class="row">
                    <h2 class="new-col-md-24">
                        <div class="features-einvoice-major-title"><?php echo get_field("tieu_dề_block_2"); ?></div>
                        <div class="features-einvoice-major-name">
                            <?php echo get_field("mo_tả_ngắn_block_2"); ?>
                        </div>
                    </h2>
                    <div class="new-col-md-24 features-einvoice-major-box">
                        <img class="img-desktop img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_2"); ?>" alt="newinvoice" />
                        <img class="img-mobile img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/Feature/img-einvoice-major-mobile.png" alt="newinvoice" />
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Khởi tạo mẫu hóa đơn -->
        <div class="features-initialize-einvoice-form">
            <div class="container">
                <div class="row">
                    <h2 class="new-col-md-24">
                        <div class="features-initialize-einvoice-form-title"><?php echo get_field("tieu_dề_block_3"); ?></div>
                        <div class="features-initialize-einvoice-form-name">
                            <?php echo get_field("mo_tả_ngắn_block_3"); ?>
                        </div>
                    </h2>
                    <div class="new-col-md-24 features-initialize-einvoice-form-box">
                        <img class="img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_3"); ?>" alt="newinvoice" />
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Xuất đơn hàng loạt -->
        <div class="features-export-series-einvoice">
            <div class="container">
                <div class="row">
                    <h2 class="new-col-md-24">
                        <div class="features-export-series-einvoice-title"><?php echo get_field("tieu_dề_block_4"); ?></div>
                        <div class="features-export-series-einvoice-name">
                            <?php echo get_field("mo_tả_ngắn_block_4"); ?>
                        </div>
                    </h2>
                    <div class="new-col-md-24 features-export-series-einvoice-box">
                        <img class="img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_4"); ?>" alt="newinvoice" />
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Tích hợp sẵn -->
        <div class="features-built-in">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24 features-built-in-box">
                        <div class="new-col-md-12 no-padding">
                            <div class="padding-left-percent">
                                <div class="features-built-in-text">
                                    <h2>
                                        <div class="features-built-in-title"><?php echo get_field("tieu_dề_block_5"); ?></div>
                                    </h2>
                                    <!-- <div class="features-built-in-detail">
                                        <div class="features-built-in-check-box">
                                            Tích hợp trực tiếp trên phần mềm kế toán MISA:
                                        </div>
                                        <div class="features-built-in-detail-sub">
                                            <div class="features-built-in-check-box-sub">
                                                Xuất hóa đơn điện tử và tự động hạch toán doanh thu vào sổ sách
                                            </div>
                                        </div>
                                        <div class="features-built-in-detail-sub">
                                            <div class="features-built-in-check-box-sub">
                                                Nhận và tự động hạch toán hóa đơn điện tử mua hàng vào sổ sách giúp tiết kiệm hơn 80% thời gian nhập liệu
                                            </div>
                                        </div>
                                    </div>
                                    <div class="features-built-in-detail">
                                        <div class="features-built-in-check-box">
                                            Kết nối với các phần mềm kế toán, bán hàng phổ biến kế thừa các thông tin bán hàng để xuất hóa đơn điện tử
                                        </div>
                                    </div>
                                    <div class="features-built-in-detail">
                                        <div class="features-built-in-check-box">
                                            Sẵn sàng kết nối với mọi phần mềm quản trị khác (phần mềm bệnh viện, nhà thuốc, siêu thị...) để xuất hóa đơn điện tử
                                        </div>
                                    </div> -->
                                    <?php echo get_field("nội_dung_block_5"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-12 no-padding features-built-in-img-box">
                            <img class="img-responsive img-lazy-load img-built-in" src="<?php echo get_field("ảnh_mo_tả_block_5"); ?>" alt="newinvoice" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Xuất hóa đơn-->
        <div class="features-export-einvoice">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24 features-export-einvoice-box">
                        <div class="new-col-md-12 no-padding">
                            <div class="features-export-einvoice-text">
                                <h2>
                                    <div class="features-export-einvoice-title"><?php echo get_field("tieu_dề_block_6"); ?></div>
                                </h2>
                                <!-- <div class="features-export-einvoice-detail">
                                    <div class="features-export-einvoice-check-box">
                                        Tức thời, nhanh chóng, tiện lợi
                                    </div>
                                </div>
                                <div class="features-export-einvoice-detail">
                                    <div class="features-export-einvoice-check-box">
                                        Nắm bắt tất cả các cơ hội kinh doanh, tránh gián đoạn công tác bán hàng
                                    </div>
                                </div> -->
                                <?php echo get_field("nội_dung_block_6"); ?>
                            </div>
                        </div>
                        <div class="new-col-md-12 no-padding features-export-einvoice-img-box">
                            <img class="img-responsive img-lazy-load img-export-einvoice" src="<?php echo get_field("ảnh_mo_tả_block_6"); ?>" alt="newinvoice" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Theo dõi hạn nợ và thanh toán -->
        <div class="features-follow-payment">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24 features-follow-payment-box">
                        <div class="new-col-md-12 no-padding">
                            <div class="padding-left-percent">
                                <div class="features-follow-payment-text">
                                    <h2>
                                        <div class="features-follow-payment-title"><?php echo get_field("tieu_dề_block_7"); ?></div>
                                    </h2>
                                    <!-- <div class="features-follow-payment-detail">
                                        <div class="features-follow-payment-check-box">
                                            Đối với người bán:
                                        </div>
                                        <div class="features-follow-payment-detail-sub">
                                            <div class="features-follow-payment-check-box-sub">
                                                Theo dõi hạn nợ của từng hóa đơn và nhắc nhở thanh toán đúng hạn, tránh phạt chậm trả
                                            </div>
                                        </div>
                                        <div class="features-follow-payment-detail-sub">
                                            <div class="features-follow-payment-check-box-sub">
                                                Tức thời nắm bắt tình hình thanh toán từng hóa đơn
                                            </div>
                                        </div>
                                    </div>
                                    <div class="features-follow-payment-detail">
                                        <div class="features-follow-payment-check-box">
                                            Đối với người mua:
                                        </div>
                                        <div class="features-follow-payment-detail-sub">
                                            <div class="features-follow-payment-check-box-sub">
                                                Thanh toán hóa đơn trực tuyến ngay trên newinvoice.vn giúp tiết kiệm thời gian, công sức
                                            </div>
                                        </div>
                                    </div> -->
                                    <?php echo get_field("nội_dung_block_7"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-12 no-padding features-follow-payment-img">
                            <img class="img-responsive img-lazy-load img-follow-payment" src="<?php echo get_field("ảnh_mo_tả_block_7"); ?>" alt="newinvoice" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!-- Quản trị tình hình sử dụng hóa đơn -->
        <div class="features-manage-einvoice-usage-title-box">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24">
                        <h2>
                            <div class="features-manage-einvoice-usage-title"><?php echo get_field("tieu_dề_block_8"); ?></div>
                            <div class="features-manage-einvoice-usage-name"><?php echo get_field("mo_tả_ngắn_block_8"); ?></div>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="features-manage-einvoice-usage">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24">
                        <div class="new-col-md-12 no-padding features-manage-einvoice-usage-box-right">
                            <!-- <div class="features-manage-einvoice-usage-box">
                                <div class="features-manage-einvoice-usage-text-first">
                                    Tổng giá trị hóa đơn đã phát hành
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="features-manage-einvoice-usage-box">
                                <div class="features-manage-einvoice-usage-text-second">
                                    Tổng số lượng hóa đơn đã phát hành
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="features-manage-einvoice-usage-box">
                                <div class="features-manage-einvoice-usage-text-third">
                                    Tình hình sử dụng hóa đơn theo thời gian
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="features-manage-einvoice-usage-box">
                                <div class="features-manage-einvoice-usage-text-fourth">
                                    Thống kê nhanh hóa đơn (chưa phát hành, hóa đơn xóa bỏ,...)
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="features-manage-einvoice-usage-box">
                                <div class="features-manage-einvoice-usage-text-fifth">
                                    Báo cáo tình hình sử dụng hóa đơn
                                </div>
                                <div class="clear"></div>
                            </div> -->
                            <?php echo get_field("nội_dung_block_8"); ?>
                        </div>
                        <div class="new-col-md-12 no-padding features-manage-einvoice-usage-box-left">
                            <img class="img-responsive img-lazy-load img-manage-einvoice-usage" src="<?php echo get_field("ảnh_mo_tả_block_8"); ?>" alt="newinvoice" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Các tính năng khác -->
        <div class="features-another-features">
            <div class="container">
                <div class="row">
                    <div class="new-col-md-24 features-another-features-box">
                        <div class="new-col-md-12 no-padding">
                            <div class="padding-left-percent">
                                <div class="features-another-features-text">
                                    <h2>
                                        <div class="features-another-features-title"><?php echo get_field("tieu_dề_block_9"); ?></div>
                                        <div class="features-another-features-name"><?php echo get_field("mo_tả_ngắn_block_9"); ?></div>
                                    </h2>
                                    <!-- <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Tự thiết kế mẫu hóa đơn theo nhu cầu
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Phân quyền sử dụng mẫu hóa đơn theo nhu cầu
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Tự thiết lập email gửi hóa đơn
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Gửi tin nhắn SMS phát hành hóa đơn và nhắc nhở thanh toán
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Hóa đơn có mã QR code phục vụ tra cứu tiện lợi
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Lập biên bản xử lý hóa đơn trên phần mềm và ký điện tử (người bán, người mua)
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Theo dõi tình trạng hóa đơn (đã gửi, khách hàng đã xem, đã thanh toán)
                                        </div>
                                    </div>
                                    <div class="features-another-features-detail">
                                        <div class="features-another-features-check-box">
                                            Các tính năng khác
                                        </div>
                                    </div> -->
                                    <?php echo get_field("nội_dung_block_9"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-12 no-padding">
                            <div class="features-another-features-img play-video" src="<?php echo get_field("video_mo_tả_block_9"); ?>">
                                <img class="img-responsive img-lazy-load img-another-features" src="<?php echo get_field("ảnh_mo_tả_block_9"); ?>" alt="newinvoice">
                                <span class="icon-play-video" src="<?php echo get_field("video_mo_tả_block_9"); ?>"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
    </div>
</div>