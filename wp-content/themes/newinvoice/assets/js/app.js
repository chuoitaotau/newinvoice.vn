/*
 * Template Name: Unify - Responsive Bootstrap Template
 * Description: Business, Corporate, Portfolio, E-commerce and Blog Theme.
 * Version: 1.6
 * Author: @htmlstream
 * Website: http://htmlstream.com
*/

var App = function () {
    //Bootstrap Tooltips and Popovers
    function handleBootstrap() {
        /*Bootstrap Carousel*/
        $('.carousel').carousel({
            interval: 15000,
            pause: 'hover'
        });

        /*Tooltips*/
        $('.tooltips').tooltip();
        $('.tooltips-show').tooltip('show');      
        $('.tooltips-hide').tooltip('hide');       
        $('.tooltips-toggle').tooltip('toggle');       
        $('.tooltips-destroy').tooltip('destroy');       

        /*Popovers*/
        $('.popovers').popover();
        $('.popovers-show').popover('show');
        $('.popovers-hide').popover('hide');
        $('.popovers-toggle').popover('toggle');
        $('.popovers-destroy').popover('destroy');
    }

    //Search Box (Header)
    function handleSearch() {    
        $('.search').click(function () {
            if($('.search-btn').hasClass('fa-search')){
                $('.search-open').fadeIn(500);
                $('.search-btn').removeClass('fa-search');
                $('.search-btn').addClass('fa-times');
            } else {
                $('.search-open').fadeOut(500);
                $('.search-btn').addClass('fa-search');
                $('.search-btn').removeClass('fa-times');
            }   
        }); 
    }

    //Sidebar Navigation Toggle
    function handleToggle() {
        $('.list-toggle').on('click', function() {
            $(this).toggleClass('active');
        });

        /*
        $('#serviceList').on('shown.bs.collapse'), function() {
            $(".servicedrop").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
        }

        $('#serviceList').on('hidden.bs.collapse'), function() {
            $(".servicedrop").addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
        }
        */
    }

    //Fixed Header
    function handleHeader() {
         $(window).scroll(function() {
            if ($(window).scrollTop()>100){
                $(".header-fixed .header-sticky").addClass("header-fixed-shrink");
            }
            else {
                $(".header-fixed .header-sticky").removeClass("header-fixed-shrink");
            }
        });
    }

    //Header Mega Menu
    function handleMegaMenu() {
        $(document).on('click', '.mega-menu .dropdown-menu', function(e) {
            e.stopPropagation()
        })
    }

    return {
        init: function () {
            handleBootstrap();
            handleSearch();
            handleToggle();
            handleHeader();
            handleMegaMenu();
        },

        //Clients Logo
        initSliders: function () {
            jQuery('#clients-flexslider').flexslider({
                animation: "slide",
                easing: "swing",
                animationLoop: true,
                itemWidth: 1,
                itemMargin: 1,
                minItems: 2,
                maxItems: 9,
                controlNav: false,
                directionNav: false,
                move: 2
            });
            
            jQuery('#clients-flexslider1').flexslider({
                animation: "slide",
                easing: "swing",
                animationLoop: true,
                itemWidth: 1,
                itemMargin: 1,
                minItems: 2,
                maxItems: 5,
                controlNav: false,
                directionNav: false,
                move: 2
            });
            
            jQuery('#photo-flexslider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                itemWidth: 80,
                itemMargin: 0
            }); 
            
            $('#testimonal_carousel').collapse({
                toggle: false
            });
        },

        //Counters 
        initCounter: function () {
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        },

        //Parallax Backgrounds
        initParallaxBg: function () {
             $(window).load(function() {
                $('.parallaxBg').parallax("50%", 0.2);
                $('.parallaxBg1').parallax("50%", 0.4);
            });
        },

    };

}();