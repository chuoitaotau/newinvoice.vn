<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newinvoice
 */

get_header();
?>
<?php get_template_part('template-parts/content','breadcrumb'); ?>
<div class="container content">
    <div class="row">
        <div class="col-sm-9">
            <div class="headline"><h1><?php single_cat_title(); ?></h1></div>
            <div class=" blog-page">
                <?php 
                  $i = 0;
                  if ( have_posts() ) : while ( have_posts() ) : the_post(); $i++;
                      $thumb_post= wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'rect_thumb');
                      $url_thumbnail  = $thumb_post['0'];                                  
                ?>  
                <!--Blog Post-->
                <div class="blog blog-medium margin-bottom-40">
                    <div class="col-sm-4 col-xs-12">
                        <img alt="<?php echo get_the_title(); ?>" src="<?php echo $url_thumbnail; ?>" class="img-responsive margin-bottom-20 lazy">
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <h4><b><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></b></h4>
                        <ul class="list-unstyled list-inline blog-info">
                            <li><i class="fa fa-calendar"></i> <?php echo get_the_date(); ?></li>
                        </ul>
                        <p class="descnt"><?php echo wp_trim_words( get_the_excerpt(), 40, '...' ); ?></p>
                    </div>
                </div>
                <!--End Blog Post-->
                    <hr class="margin-bottom-40">
                <?php endwhile; else: ?>
                <?php echo _e('Nội dung đang được cập nhật...'); ?>
                <?php endif; ?><?php wp_reset_postdata();?>     
                <div class="clearfix"></div>
                <?php wp_pagenavi(); ?>
            </div><!--/row-->
        </div>

        <?php echo get_sidebar(); ?>
    </div>

    <?php get_template_part('template-parts/content','customers'); ?>
</div>
<?php get_template_part('template-parts/content','popup'); ?>
<!--=== End PageMainContent ===-->
<?php
get_footer();
