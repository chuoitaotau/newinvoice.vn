<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newinvoice
 */

get_header();
?>
<?php get_template_part('template-parts/content','breadcrumb'); ?>
<div class="container content content-new-detail">
    <div class="margin-bottom-40">
        <div class="row">
            <div class="col-md-8">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="title-box-v2">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
                <div class="textcnt">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; else: ?>
	            <?php echo _e('Nội dung đang được cập nhật...'); ?>
	            <?php endif; ?><?php wp_reset_postdata();?>
            </div>
            <?php get_template_part('template-parts/content','sidebarPage'); ?>
        </div>
    </div>

    <?php get_template_part('template-parts/content','customers'); ?>
</div>
<?php
get_footer();