<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newinvoice
 */
global $data;
?>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title('');?></title>
    <link rel="shortcut icon" href="<?php echo $data['favicon_img']; ?>"/>
    <link rel="icon" href="<?php echo $data['favicon_img']; ?>" sizes="192x192" />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/site.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/flexslider.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/headers/header-default.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/footers/footer-default.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/footers/footer-v1.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/app.css" rel="stylesheet"/>
    <link href="<?php bloginfo('template_url');?>/assets/css/lightbox/prettyPhoto.css" rel="stylesheet"/>
    <script src="<?php bloginfo('template_url');?>/assets/js/modernizr-2.6.2.js"></script>
    <link href="<?php bloginfo('template_url');?>/assets/css/bootstrapValidator.css" rel="stylesheet"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url');?>/assets/images/icon.ico">
    <link href="<?php bloginfo('template_url');?>/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/bootstrapValidator.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/custom.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/price-page.css" rel="stylesheet" />
    
    <script src="<?php bloginfo('template_url');?>/assets/js/jquery-1.10.2.js"></script>

    <script src="<?php bloginfo('template_url');?>/assets/js/bootstrap.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/BSS.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/owl.carousel.min.js"></script>

    <?php wp_head(); ?>
    <!-- css/js hddt -->
    <link href="<?php bloginfo('template_url');?>/assets/css/page-hddt.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/dkdl-page.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/feature-page.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url');?>/assets/css/owl.theme.default.min.css" rel="stylesheet" />
    <script src="<?php bloginfo('template_url');?>/assets/js/hddt.js"></script>
    <script src="<?php bloginfo('template_url');?>/assets/js/owl.carousel.min.js"></script>
    
    <?php echo $data['google_analytics']; ?>
    <?php echo $data['google_webmaster_tool']; ?>
</head>
<body <?php body_class(); ?>>
	<?php echo $data['remarketing_adwords']; ?>
    <div class="wrapper">
        <!--=== WP-Header ===-->
        <div class="header">
            <!-- Navbar -->
            <div role="navigation" class="navbar navbar-default mega-menu">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button data-target=".navbar-responsive-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="fa fa-bars"></span>
                        </button>
                        <a href="<?php echo home_url(); ?>" class="navbar-brand">
                            <img width="80" alt="" src="<?php echo $data['logo_img']; ?>" id="logo-header">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <?php if(function_exists('wp_nav_menu')){wp_nav_menu( 'theme_location=menu-primary&menu_class=nav navbar-nav main-menu&container=""');} ?>
                    </div>
                </div>
            </div><!--/navbar-collapse-->
        </div>
         <!--=== End WP-Header ===-->
