<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<div class="price-page">
  <!-- Báo giá -->
  <div class="home-price">
      <div class="container">
          <div class="row">
              <h2>
                  <div class="home-price-title"><?php echo get_field("tieu_dề_block_bao_gia"); ?></div>
                  <div class="home-price-name"><?php echo get_field("mo_tả_ngắn_block_bao_gia"); ?></div>
              </h2>
              <div class="new-col-md-24 no-padding">
                <?php
                    $i = 0;
                    if( have_rows('quản_ly_goi_hoa_don') ):
                        while ( have_rows('quản_ly_goi_hoa_don') ) : the_row(); $i++;
                        if($i == 1){
                ?>
                  <div class="new-col-md-6 new-col-md-offset-3 price-box-padding-first">
                      <div class="price-box-first">
                          <div class="box-first-title">
                              <div class="first-mei-title">
                                  <?php echo get_sub_field('ma_goi_hoa_don'); ?>
                              </div>
                              <div class="first-price-title">
                                  <?php echo get_sub_field('gia_goi_hoa_don'); ?>
                              </div>
                          </div>
                          <div class="box-first-content">
                              <div class="content-box">
                                  <div class="content-number-invoice">
                                      <?php echo get_sub_field('số_hoa_don_trong_goi'); ?>
                                  </div>
                              </div>
                              <div class="content-number">
                                  <?php echo get_sub_field('gia_tiền_mỗi_hoa_don'); ?>
                              </div>
                              <div class="content-unit">
                                  VNĐ/Hóa đơn
                              </div>
                              <a target="_blank" href="<?php echo get_sub_field('link_mua_ngay'); ?>">
                                  <div class="price-btn-buy-now">
                                      Mua ngay
                                  </div>
                              </a>
                          </div>
                      </div>
                  </div>
                <?php } else if($i == 2){ ?>
                  <div class="new-col-md-6 price-box-padding-second">
                      <div class="price-box-second">
                          <div class="box-second-title">
                              <div class="second-mei-title">
                                  <?php echo get_sub_field('ma_goi_hoa_don'); ?>
                              </div>
                              <div class="second-price-title">
                                  <?php echo get_sub_field('gia_goi_hoa_don'); ?>
                              </div>
                          </div>
                          <div class="box-second-content">
                              <div class="content-box">
                                  <div class="content-number-invoice">
                                      <?php echo get_sub_field('số_hoa_don_trong_goi'); ?>
                                  </div>
                              </div>
                              <div class="content-number">
                                  <?php echo get_sub_field('gia_tiền_mỗi_hoa_don'); ?>
                              </div>
                              <div class="content-unit">
                                  VNĐ/Hóa đơn
                              </div>
                              <a target="_blank" href="<?php echo get_field('link_mua_ngay'); ?>">
                                  <div class="price-btn-buy-now">
                                      Mua ngay
                                  </div>
                              </a>
                          </div>
                      </div>
                  </div>
                <?php } else if($i == 3){ ?>
                  <div class="new-col-md-6 price-box-padding-third">
                      <div class="price-box-third">
                          <div class="box-third-title">
                              <div class="third-mei-title">
                                  <?php echo get_sub_field('ma_goi_hoa_don'); ?>
                              </div>
                              <div class="third-price-title">
                                  <?php echo get_sub_field('gia_goi_hoa_don'); ?>
                              </div>
                          </div>
                          <div class="box-third-content">
                              <div class="content-box">
                                  <div class="content-number-invoice">
                                      <?php echo get_sub_field('số_hoa_don_trong_goi'); ?>
                                  </div>
                              </div>
                              <div class="content-number">
                                  <?php echo get_sub_field('gia_tiền_mỗi_hoa_don'); ?>
                              </div>
                              <div class="content-unit">
                                  VNĐ/Hóa đơn
                              </div>
                              <a target="_blank" href="<?php echo get_sub_field('link_mua_ngay'); ?>">
                                  <div class="price-btn-buy-now">
                                      Mua ngay
                                  </div>
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
              <?php } ?>
              <?php  
                  endwhile;
              endif;
              ?>
              <div class="new-col-md-24">
                  <div class="price-btn-fee">
                      <span class="price-btn-fee-text"><?php echo get_field("phi_thue_bao_hang_nam"); ?></span>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!---->

  <div class="price-service">
      <div class="container">
          <div class="row">
              <div class="new-col-md-24">
                  <div class="other-product">
                      <div class="other-product-header">
                          Dịch vụ
                      </div>
                      <?php
                          $i = 0;
                          if( have_rows('quản_ly_dịch_vụ') ):
                              while ( have_rows('quản_ly_dịch_vụ') ) : the_row(); $i++;

                          $class = "edit-template";
                          if($i==2){
                            $class = "publish-invoice";
                          }
                      ?>
                      <div class="op-item <?php echo $class; ?>">
                          <div class="other-product-title item-last">
                              <!-- <span>thiết lập </span>
                              <span>mẫu hóa đơn</span> -->
                              <?php echo get_sub_field('ten_dịch_vụ'); ?>
                          </div>
                          <div class="other-product-item-container">
                              <?php
                                  $j = 0;
                                  if( have_rows('chi_tiết_dịch_vụ') ):
                                      while ( have_rows('chi_tiết_dịch_vụ') ) : the_row(); $j++;
                              ?>
                              <div class="other-product-item">
                                  <div class="other-product-price">
                                      <?php echo get_sub_field('tieu_dề_dịch_vụ'); ?>
                                  </div>
                                  <div class="other-product-content">
                                      <!-- <ul>
                                          <li>Khách hàng dùng mẫu mặc định của MISA</li>
                                          <li>Khách hàng tự thay đổi logo, hình nền, font chữ, cỡ chữ...</li>
                                      </ul> -->
                                      <?php echo get_sub_field('mo_tả_dịch_vụ'); ?>
                                  </div>
                              </div>
                              <?php  
                                  endwhile;
                              endif;
                              ?> 
                              <!-- <div class="other-product-item">
                                  <div class="other-product-price">
                                      2.000.000 VNĐ
                                  </div>
                                  <div class="other-product-content">
                                      <ul>
                                          <li>Khách hàng dùng mẫu mặc định của MISA</li>
                                          <li>MISA hỗ trợ sửa mẫu hóa đơn trên mẫu mặc định sẵn có</li>
                                      </ul>

                                  </div>
                              </div>
                              <div class="other-product-item">
                                  <div class="other-product-price item-last">
                                      5.000.000 VNĐ
                                  </div>
                                  <div class="other-product-content item-last">
                                      <ul>
                                          <li>Khách hàng dùng mẫu đặc thù của đơn vị</li>
                                          <li>MISA hỗ trợ sửa mẫu theo yêu cầu đặc thù</li>
                                      </ul>
                                  </div>
                              </div> -->
                          </div>
                      </div>
                      <?php  
                          endwhile;
                      endif;
                      ?> 
                      <!-- <div class="op-item publish-invoice">
                          <div class="other-product-title item-last">
                              <span>tư vấn thủ tục</span>
                              <span>phát hành hóa đơn</span>
                          </div>
                          <div class="other-product-item-container">

                              <div class="other-product-item">
                                  <div class="other-product-price item-last">
                                      3.000.000 VNĐ
                                  </div>
                                  <div class="other-product-content item-last">
                                      <ul>
                                          <li>Tư vấn, hướng dẫn các thủ tục lập hồ sơ để thông báo phát hành hóa đơn điện tử với cơ quan thuế</li>
                                          <li>Khách hàng trực tiếp đăng ký thủ tục với cơ quan thuế</li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div> -->
                  </div>
                  <div class="btn-buy-now-box">
                      <a class="btn-buy-now" href="<?php echo get_field('link_mua_ngay'); ?>" target="_blank"> Mua ngay</a>
                  </div>
              </div>
              <div class="new-col-md-24 note-box">
                  <div class="new-col-md-24 service-note-title">
                      Lưu ý
                  </div>
                  <div class="new-col-md-24 no-padding">
                      <div class="new-col-md-13">
                          <div class="service-note-content">
                             <?php echo get_field('luu_y'); ?>
                          </div>
                      </div>
                      <div class="new-col-md-11">
                          <div class="price-service-button-box">
                              <a class="button-download-detail-quotes" target="_blank" href="<?php echo get_field('link_tải_bao_gia_chi_tiết'); ?>">
                                  Tải báo giá chi tiết
                              </a>
                              <a class="button-download-service-agreement" target="_blank" href="<?php echo get_field('link_thỏa_thuận_dịch_vụ'); ?>">
                                  Thỏa thuận sử dụng dịch vụ
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="price-films">
      <div class="container">
          <div class="row">
              <h2 class="new-col-md-24">
                  <div class="price-films-title">Phim hướng dẫn mua hàng</div>
              </h2>
              <div class="new-col-md-24 no-padding price-films-content">
                  <?php
                      $i = 0;
                      if( have_rows('quản_ly_video_huớng_dẫn_mua_hang') ):
                          while ( have_rows('quản_ly_video_huớng_dẫn_mua_hang') ) : the_row(); $i++;

                      $tieude_video = get_sub_field('tieu_dề_video'); 
                      $attachment_id = get_sub_field('ảnh_dại_diện_bai_viết'); 
                      $link_video = get_sub_field('link_video');
                      $link_post = get_sub_field('link_chi_tiết_bai_viết'); 

                      $size = "rect_thumb"; // (thumbnail, medium, large, full or custom size)
                      $image = wp_get_attachment_image_src( $attachment_id, $size );
                      $thumbnail = $image[0]; 
                      if($i == 1){ $class_video = "new-col-md-offset-4 price-films-box-first"; }else{ $class_video = ""; }
                  ?>
                  <div class="new-col-md-8 <?php echo $class_video; ?>">
                      <div class="price-films-box">
                          <div class="price-films-box-image play-video" src="<?php echo $link_video; ?>">
                              <img class="img-responsive" src="<?php echo $thumbnail; ?>" alt="newinvoice" />
                              <span class="icon-play-video"></span>
                          </div>
                          <div class="price-films-box-text">
                              <div class="price-films-box-title">
                                  <?php echo $tieude_video; ?>
                              </div>
                              <div class="price-films-box-read-more">
                                  <a href="<?php echo $link_post; ?>" target="_blank">Xem chi tiết</a>
                              </div>
                          </div>
                      </div>
                  </div>
                  <?php  
                      endwhile;
                  endif;
                  ?>
                  <!-- <div class="new-col-md-8">
                      <div class="price-films-box">
                          <div class="price-films-box-image play-video" src="https://player.vimeo.com/video/329287952">
                              <img class="img-responsive" src="<?php bloginfo('template_url');?>/assets/images/img-buy-misa.jpg" alt="newinvoice" />
                              <span class="icon-play-video"></span>
                          </div>
                          <div class="price-films-box-text">
                              <div class="price-films-box-title">
                                  Mua hàng trên phần mềm MISA
                              </div>
                              <div class="price-films-box-read-more">
                                  <a href="http://help.newinvoice.vn/html_6000001.htm" target="_blank">Xem chi tiết</a>
                              </div>
                          </div>
                      </div>
                  </div> -->
              </div>
          </div>
      </div>
  </div>

  <div class="price-questions">
      <div class="container">
          <div class="row">
              <h2 class="new-col-md-24">
                  <div class="price-questions-title">Câu hỏi thường gặp khi mua hàng</div>
              </h2>
              <div class="new-col-md-24 price-questions-content">
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <!--1-->
                      <?php
                          $i = 0;
                          if( have_rows('quản_ly_cau_hỏi_thuờng_gặp') ):
                              while ( have_rows('quản_ly_cau_hỏi_thuờng_gặp') ) : the_row(); $i++;

                              $cauhoi = get_sub_field('cau_hỏi'); 
                              $dapan = get_sub_field('dap_an');
                      ?>
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                      <em class="more-less glyphicon glyphicon-minus"></em>
                                      <div class="content-question">
                                          <?php echo $cauhoi; ?> 
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse<?php echo $i; ?>" class="panel-collapse <?php if($i==1){ echo 'in'; }else{ echo 'collapse'; } ?>" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>" style="height: auto;">
                              <div class="panel-body">
                                  <?php echo $dapan; ?> 
                              </div>
                          </div>
                      </div>
                      <?php  
                          endwhile;
                      endif;
                      ?>
                      
                      <!-- <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading3">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Để sử dụng phần mềm hóa đơn điện tử của MISA tôi có cần phải đầu tư thêm trang thiết bị gì không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                              <div class="panel-body">
                                  Không. Phần mềm được phát triển trên nên tảng công nghệ điện toán đám mây nên Qúy khách chỉ cần đăng ký sử dụng là có thể sử dụng được ngay.
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading5">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Sử dụng phần mềm hóa đơn của MISA hóa đơn có an toàn, bảo mật không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                              <div class="panel-body">
                                  - newinvoice.vn là phần mềm hóa đơn điện tử duy nhất tại Việt Nam ứng dụng công nghệ Blockchain giúp chống giả mạo hóa đơn, gia tăng độ tin cậy cho hóa đơn và doanh nghiệp.
                                  <br />
                                  - Hóa đơn được lưu trữ tại trung tâm lưu trữ dữ liệu của MISA đạt tiêu chuẩn quốc tế ISO 27000, CSA STAR nên Khách hàng có thể hoàn toàn yên tâm về tính an toàn, bảo mật của dữ liệu.
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading7">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp tôi sử dụng một số lượng lớn hóa đơn/năm, vậy MISA có chính sách giá ưu đãi gì khi mua lượng lớn hóa đơn không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                              <div class="panel-body">
                                  MISA có chính sách ưu đãi cho những Quý khách hàng khi mua số lượng lớn hóa đơn.
                                  <br />
                                  Ví dụ:
                                  <br />
                                  + DN dùng 2.000 hóa đơn thì đơn giá/ 1HĐ là 400 đ
                                  <br />
                                  + DN dùng 10.000 hóa đơn thì đơn giá/ 1HĐ là 300 đ
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading9">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp tôi dự kiến sử dụng 2.300 hóa đơn/ năm, tôi có thể đặt mua 2.300 hóa đơn không và phải trả bao nhiêu tiền?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                              <div class="panel-body">
                                  Phần mềm newinvoice.vn được bán theo các gói cố định 500 hóa đơn, 2.000 hóa đơn và 10.000 hóa đơn. Khi mua hàng, Quý khách vui lòng đặt mua theo bội số của những gói hóa đơn đó:
                                  <br />
                                  VD: Khi lượng hóa đơn phát sinh của Quý khách khoảng 2.300 hóa đơn/ năm, Quý khách có thể đặt mua 1 gói 2.000 hóa đơn và 1 gói 500 hóa đơn, số tiền phải trả:
                                  <br />
                                  + Phí thuê bao: 1.000.000 đ/ năm
                                  <br />
                                  + Phí sử dụng hóa đơn : 1 x 250.000 + 1 x 800.000 = 1.050.000 đ
                                  <br />
                                  => Tổng phải trả: 2.050.000 đ cho 1 năm sử dụng với 2.500 hóa đơn
                                  <br />
                                  Tuy nhiên, số hóa đơn doanh nghiệp chưa sử dụng hết trong năm thì sẽ được chuyển sang năm tài chính tiếp theo để dùng tiếp
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading2">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Tôi xem báo giá thấy có 2 loại phí là phí thuê bao và phí sử dụng hóa đơn điện tử, tôi chưa hiểu phí thuê bao là gì?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                              <div class="panel-body">
                                  Phí thuê bao hàng năm là khoản phí bảo trì và duy trì hệ thống phần mềm hoạt động liên tục (như hạ tầng, đường truyền, phí lưu trữ an toàn, con người,...)
                                  <br />
                                  VD: Quý khách dùng điện thoại cố định, các nhà cung cấp dịch vụ cũng thu một khoản cước thuê bao điện thoại cố định,VD: Viettel: 22.000 đ/ tháng
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading4">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp tôi có tổng công ty và 3 chi nhánh, mỗi chi nhánh có mã số thuế và có nhu cầu xuất hóa đơn riêng theo từng chi nhánh, vậy tôi phải mua bao nhiêu tài khoản để sử dụng newinvoice.vn?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                              <div class="panel-body">
                                  Doanh nghiệp có tổng công ty và 3 chi nhánh có mã số thuế riêng thì doanh nghiệp phải mua 4 tài khoản để sử dụng newinvoice.vn
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading6">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp tôi có 3 chi nhánh phụ thuộc, tổng công ty thông báo phát hành hóa đơn và phân bổ cho các chi nhánh phụ thuộc sử dụng, vậy tôi phải mua bao nhiêu tài khoản để sử dụng newinvoice.vn?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                              <div class="panel-body">
                                  Doanh nghiệp có 3 chi nhánh phụ thuộc thì doanh nghiệp chỉ phải mua 1 tài khoản sử dụng newinvoice.vn
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading8">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp tôi đang sử dụng newinvoice.vn, giờ muốn thay đổi thông tin về địa chỉ của công ty nhưng không thay đổi mã số thuế thì tôi có tiếp tục sử dụng được không, có phải trả khoản phí nào không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                              <div class="panel-body">
                                  Doanh nghiệp không thay đổi Mã số thuế, chỉ thay đổi thông địa chỉ thì vẫn dùng được newinvoice.vn, và không phải trả thêm khoản phí nào phí để thay đổi thông tin.
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading10">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp tôi mua 500 hóa đơn điện tử sử dụng, nhưng năm sau tôi không dùng nữa thì tôi có phải trả tiền không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                              <div class="panel-body">
                                  Nếu doanh nghiệp đang sử dụng newinvoice.vn khi hết hạn thuê bao nhưng không gia hạn để sử dụng tiếp thì doanh nghiệp không phải trả thêm bất cứ một khoản phí nào nữa, tuy nhiên số lượng hóa đơn mà doanh nghiệp mua trước đó nếu chưa sử dụng hết thì sẽ không được sử dụng tiếp.
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading11">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Doanh nghiệp được bảo hành sản phẩm MISA SME.NET đến tháng 12/2018 hết hạn, và đã trả tiền để sử dụng hóa đơn điện tử của MISA tới tháng 3/2019 mới hết hạn, nếu doanh nghiệp không mua phí cập nhật cho MISA SME.NET sau khi hết hạn bảo hành (sau 31/12/2018) thì có sử dụng được hóa đơn điện tử của MISA không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                              <div class="panel-body">
                                  Doanh nghiệp sẽ vẫn sử dụng được newinvoice.vn nến vẫn còn thời hạn thuê bao và vẫn còn số lượng hóa đơn để sử dụng. Tuy nhiên, doanh nghiệp nên mua phí cập nhật của sản phẩm MISA SME.NET để nếu chính sách về hóa đơn, hay chính sách thuế của nhà nước thay đổi thì doanh nghiệp sẽ được cập nhật lên phiên bản mới nhất để sử dụng mà không mất thêm bất kỳ một khoản phí nâng cấp nào.
                              </div>
                          </div>
                      </div>
                      
                      <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading12">
                              <h4 class="panel-title">
                                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                      <em class="more-less glyphicon glyphicon-plus"></em>
                                      <div class="content-question">
                                          Năm nay tôi mua 1.000 hoá đơn nhưng dùng hết 800 tờ hoá đơn, sau khi hết hạn  thuê bao tôi không gia hạn thuê bao tiếp thì 200 tờ hoá đơn còn lại có được sử dụng tiếp không?
                                      </div>
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                              <div class="panel-body">
                                  Nếu doanh nghiệp hết hạn thuê bao, doanh nghiệp không gia hạn thuê bao nữa thì sẽ không phát hành hóa đơn trên newinvoice.vn được, dù doanh nghiệp còn số lượng hóa đơn đã mua nhưng chưa sử dụng hết.
                              </div>
                          </div>
                      </div> -->
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>