<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<!-- PageMainContent -->
<div class="home-page">
    <!-- Banner -->
    <!-- <div id="Slider" class="carousel slide carousel-v1 box-shadow shadow-effect-2">
        <ol class="carousel-indicators">
            <li data-slide-to="0" data-target="#Slider" class="rounded-x active"></li>
            <li data-slide-to="1" data-target="#Slider" class="rounded-x"></li>
        </ol>
        <div class="carousel-inner home-header-banner home-item">
            <div class="item active item carousel-inner">
                <div class="background-homebanner">
                    <div class="container">
                        <div class="new-col-md-24 no-padding">
                            <div class="new-col-md-12 left-conttent no-padding">
                                <div class="new-col-md-24 no-padding content-home-banner">
                                    <div class="banner-logo">
                                        meInvoice.vn
                                    </div>
                                    <div class="banner-line-vertical"></div>
                                    <div class="banner-title">
                                        Phần mềm hóa đơn điện tử
                                    </div>
                                    <div class="banner-title-yellow">
                                        Được ưa chuộng nhất
                                    </div>
                                    <div class="banner-sub">
                                        MISA - <span class="text-bold">25</span> năm kinh nghiệm trong lĩnh vực phát triển phần mềm kế toán, hóa đơn điện tử, kê khai thuế,... cho hơn <span class="text-bold">200.000</span> tổ chức.
                                    </div>
                                </div>
                                <div class="new-col-md-24 no-padding buttom-home-banner">
                                    <div class="button-home">
                                        <a href="don-hang/index.html" target="_blank" class="new-col-md-12 buy-now">Mua ngay</a>
                                        <div src="https://player.vimeo.com/video/328128345" class="new-col-md-12 video-demo play-video">Xem demo</div>
                                        <div class="free-trial-button-home">
                                            <a href="https://sign-up.meinvoice.vn/" target="_blank" class="new-col-md-24 free-trial">Dùng thử</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item item carousel-inner">
                 <div class="background-homebanner-second">
                    <div class="container">
                        <div class="new-col-md-24 no-padding">
                            <div class="new-col-md-14 left-conttent-second no-padding">
                                <div class="new-col-md-24 no-padding content-home-banner-second">
                                    <div class="banner-title-second">
                                        meInvoice.vn
                                    </div>
                                    <div class="banner-sub-second">
                                        Phần mềm Hóa đơn điện tử <br />
                                        <span class="text-bold">duy nhất</span> lập hóa đơn ngay trên Mobile
                                    </div>
                                </div>
                                <div class="new-col-md-24 no-padding buttom-home-banner-second">
                                    <div class="button-home-second">
                                        <a href="don-hang/index.html" target="_blank" class="new-col-md-12 buy-now-second">Mua ngay</a>
                                        <div src="https://player.vimeo.com/video/328128345" class="new-col-md-12 video-demo-second play-video">Xem demo</div>
                                        <div class="free-trial-button-home">
                                            <a href="https://sign-up.meinvoice.vn/" target="_blank" class="new-col-md-24 free-trial-second">Dùng thử</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="new-col-md-24 no-padding home-banner-horizontal-line"></div>
                                <div class="new-col-md-24 no-padding home-banner-qr-code">
                                    <div class="home-banner-qr-code-image-right">
                                        <img src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-qr-code.svg" />
                                    </div>
                                    <div class="home-banner-qr-code-image-left">
                                        <div class="home-banner-qr-code-images">
                                            <a href="https://play.google.com/store/apps/details?id=vn.com.misa.meinvoice" target="_blank">
                                                <img class="home-banner-qr-code-image-first" src="<?php bloginfo('template_url');?>/assets/images/hddt/img-white-google-play.svg" alt="newinvoice" />
                                            </a>
                                            <a href="https://itunes.apple.com/app/meinvoice/id1460947129?mt=8" target="_blank">
                                                <img class="home-banner-qr-code-image-second" src="<?php bloginfo('template_url');?>/assets/images/hddt/img-white-apple-store.svg" alt="newinvoice" />
                                            </a>
                                        </div>
                                        <div class="home-banner-qr-code-text">Quét mã QR code để cài đặt phần mềm</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-arrow">
            <a class="left carousel-control" href="#Slider" data-slide="prev">
                <i class="fa-chevron-circle-left fa"></i>
            </a>
            <a class="right carousel-control" href="#Slider" data-slide="next">
                <i class="fa-chevron-circle-right fa"></i>
            </a>
        </div>
    </div>   -->
    <?php 
        $slider = get_field("shortcode_slider");
        echo do_shortcode($slider); 
    ?>
    <h1 class="text-hide">
        NewInvoice.vn - Phần mềm Hóa đơn điện tử an toàn nhất, dễ sử dụng nhất
    </h1>
    <!-- Nghị định -->
    <div class="home-decree">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24">
                    <div class="home-decree-box">
                        <span class="text-bold"><span class="text-uppercase"><?php echo get_field("thong_bao"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- Giới thiệu -->
    <div class="home-introduce">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24 home-introduce-box">
                    <div class="new-col-md-12 no-padding">
                        <div class="home-introduce-img play-video-introduce play-video" src="<?php echo get_field("video_block_giới_thiệu"); ?>">
                            <iframe width="560" height="315" src="<?php echo get_field("video_block_giới_thiệu"); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="new-col-md-12 no-padding">
                        <div class="home-introduce-text">
                            <h2>
                                <div class="home-introduce-title"><?php echo get_field("tieu_dề_block_giới_thiệu"); ?></div>
                                <div class="home-introduce-name"><?php echo get_field("mo_tả_ngắn_block_giới_thiệu"); ?></div>
                            </h2>
                            <?php echo get_field("nội_dung_block_giới_thiệu"); ?>
                            <!-- <div class="home-introduce-detail">
                                <div class="box-check-introduce">
                                    Đáp ứng đầy đủ nghiệp vụ hóa đơn theo đúng Nghị định 119/2018/NĐ-CP, Thông tư 32/2011/TT-BTC, Thông tư 39/2014/TT-BTC...
                                </div>
                            </div>
                            <div class="home-introduce-detail">
                                <div class="box-check-introduce">
                                    Người mua tức thời nhận được hóa đơn điện tử qua email, SMS và thực hiện tra cứu hóa đơn mọi lúc, mọi nơi qua internet
                                </div>
                            </div>
                            <div class="home-introduce-detail">
                                <div class="box-check-introduce">
                                    Sẵn sàng kết nối với mọi phần mềm kế toán, bán hàng và các phần mềm quản trị khác để phát hành hóa đơn điện tử
                                </div>
                            </div>
                            <div class="home-introduce-detail">
                                <div class="box-check-introduce">
                                    Phần mềm hóa đơn điện tử duy nhất tại Việt Nam ứng dụng công nghệ Blockchain đảm bảo an toàn, bảo mật và chống làm giả hóa đơn
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- Lợi ích -->
    <div class="home-benefit">
        <div class="container">
            <div class="row">
                <h2>
                    <div class="home-benefit-title"><?php echo get_field("tieu_dề_block_lợi_ich"); ?></div>
                    <div class="home-benefit-name"><?php echo get_field("mo_tả_ngắn_block_lợi_ich"); ?></div>
                </h2>
                <div class="new-col-md-24 no-padding home-benefit-box">
                    <?php
                        $i = 0;
                        if( have_rows('quản_ly_block_lợi_ich') ):
                            while ( have_rows('quản_ly_block_lợi_ich') ) : the_row(); $i++;

                        $anh_mota = get_sub_field('ảnh_mo_tả'); 
                        $tieu_de_loiich = get_sub_field('tieu_dề'); 
                        $noidung_loiich = get_sub_field('nội_dung');
                    ?>
                    <div class="new-col-md-6 home-benefit-box-padding-top">
                        <div class="home-benefit-item">
                            <div class="benefit-img">
                                <img class="img-responsive" src="<?php echo $anh_mota; ?>" data-src="<?php echo $anh_mota; ?>" alt="newinvoice" />
                            </div>
                            <div class="benefit-item-title"><?php echo $tieu_de_loiich; ?></div>
                            <div class="benefit-line-vertical"></div>
                            <div class="benefit-item-detail"><?php echo $noidung_loiich; ?></div>
                        </div>
                    </div>
                    <?php  
                        endwhile;
                    endif;
                    ?> 
                    <!-- <div class="new-col-md-6 home-benefit-box-padding-top">
                        <div class="home-benefit-item">
                            <div class="benefit-img">
                                <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/img-benefit-2.svg" alt="newinvoice" />
                            </div>
                            <div class="benefit-item-title">An toàn</div>
                            <div class="benefit-line-vertical"></div>
                            <div class="benefit-item-detail">Không xảy ra cháy, hỏng, mất hóa đơn. Tăng độ tin cậy cho hóa đơn của doanh nghiệp</div>
                        </div>
                    </div>
                    <div class="new-col-md-6 home-benefit-box-padding-top">
                        <div class="home-benefit-item">
                            <div class="benefit-img">
                                <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/img-benefit-3.svg" alt="newinvoice" />
                            </div>
                            <div class="benefit-item-title">Nhanh chóng</div>
                            <div class="benefit-line-vertical"></div>
                            <div class="benefit-item-detail">Người mua tức thời nhận được hóa đơn, rút ngắn thời gian thu nợ</div>
                        </div>
                    </div>
                    <div class="new-col-md-6 home-benefit-box-padding-top">
                        <div class="home-benefit-item">
                            <div class="benefit-img">
                                <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/img-benefit-4.svg" alt="newinvoice" />
                            </div>
                            <div class="benefit-item-title">Bảo vệ môi trường</div>
                            <div class="benefit-line-vertical"></div>
                            <div class="benefit-item-detail">Giảm bớt việc sử dụng giấy, góp phần bảo vệ môi trường</div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!--Blockchain-->
    <div class="home-blockchain">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24 home-blockchain-box">
                    <div class="new-col-md-12 no-padding">
                        <div class="home-introduce-img play-video-blockchain play-video" src="<?php echo get_field("video_block_tien_phong"); ?>">
                            <iframe width="560" height="315" src="<?php echo get_field("video_block_tien_phong"); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="new-col-md-12 no-padding">
                        <div class="padding-left-20-percent">
                            <!-- <h2>
                                <div class="home-introduce-title">Tiên phong ứng dụng công nghệ chống giả mạo hóa đơn</div>
                                <div class="home-introduce-name">Blockchain</div>
                            </h2>
                            <div class="home-introduce-detail">
                                Blockchain lưu trữ lại tất cả những thay đổi liên quan đến hóa đơn dưới dạng các liên kết chuỗi khối không thể tác động, phá vỡ giúp đảm bảo an toàn, bảo mật và chống làm giả hóa đơn
                            </div> -->
                            <?php echo get_field("nội_dung_block_tien_phong"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- Điểm ưu việt -->
    <div class="home-superior-points-title-box">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24 no-padding">
                    <h2>
                        <div class="home-superior-points-title"><?php echo get_field("tieu_dề_block_diểm_uu_việt"); ?></div>
                        <div class="home-superior-points-name"><?php echo get_field("mo_tả_ngắn_block_diểm_uu_việt"); ?></div>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="home-superior-points">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24">
                    <div class="new-col-md-12 no-padding home-superior-points-box-right">
                        <!-- <div class="superior-points-box">
                            <div class="superior-points-number">
                                01.
                            </div>
                            <div class="superior-points-text">
                                Khởi tạo và phát hành hóa đơn điện tử mọi lúc, mọi nơi qua mobile, website, desktop tránh gián đoạn công tác bán hàng
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="superior-points-box">
                            <div class="superior-points-number">
                                02.
                            </div>
                            <div class="superior-points-text">
                                Sẵn sàng kết nối với mọi phần mềm kế toán, quản trị khác hỗ trợ xuất hóa đơn điện tử nhanh chóng, tiện lợi
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="superior-points-box">
                            <div class="superior-points-number">
                                03.
                            </div>
                            <div class="superior-points-text">
                                Tích hợp sẵn trên phần mềm MISA để xuất và tự động hạch toán hóa đơn điện tử, nhận và hạch toán hóa đơn điện tử mua hàng
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="superior-points-box">
                            <div class="superior-points-number">
                                04.
                            </div>
                            <div class="superior-points-text">
                                Theo dõi hạn nợ và thanh toán hóa đơn trực tuyến ngay trên mobile tránh bị phạt trả chậm
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="superior-points-box">
                            <div class="superior-points-number">
                                05.
                            </div>
                            <div class="superior-points-text">
                                Cung cấp sẵn các mẫu hóa đơn với đầy đủ thông tin theo quy định của Cơ quan Thuế và thiết kế lại mẫu theo nhu cầu
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="superior-points-box">
                            <div class="superior-points-number">
                                06.
                            </div>
                            <div class="superior-points-text">
                                Tự động tổng hợp Tờ khai thuế GTGT và Báo cáo tình hình sử dụng hóa đơn, xuất dữ liệu phục vụ kê khai thuế
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="superior-points-box">
                            <div class="superior-points-number">
                                07.
                            </div>
                            <div class="superior-points-text">
                                Tra cứu hóa đơn trên mobile mọi lúc, mọi nơi. Lưu trữ hóa đơn trong 10 năm tại trung tâm lưu trữ dữ liệu chuẩn quốc tế
                            </div>
                            <div class="clear"></div>
                        </div> -->
                        <?php echo get_field("nội_dung_block_diểm_uu_việt"); ?>
                    </div>
                    <div class="new-col-md-12 no-padding home-superior-points-box-left">
                        <img class="img-responsive img-lazy-load" src="<?php echo get_field("ảnh_mo_tả_block_diểm_uu_việt"); ?>" alt="newinvoice" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- Lý do -->
    <div class="home-reasons">
        <div class="container">
            <div class="row">
                <div class="new-col-md-24 no-padding">
                    <div class="new-col-md-4 text-align-center">
                        <img class="img-home-reasons img-responsive img-lazy-load" src="<?php echo get_field("ảnh_tieu_dề_block_ly_do_chọn"); ?>" alt="newinvoice" />
                    </div>
                    <div class="new-col-md-10 home-reasons-left">
                        <?php
                            $i = 0;
                            if( have_rows('quản_ly_block_ly_do_chọn') ):
                                while ( have_rows('quản_ly_block_ly_do_chọn') ) : the_row(); $i++;

                            $icon_ly_do_chon = get_sub_field('icon_ly_do_chọn'); 
                            $tieude_lydo = get_sub_field('tieu_dề');
                            $noidung_lydo = get_sub_field('nội_dung'); 
                        ?>
                        <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php echo $icon_ly_do_chon; ?>" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title"><?php echo $tieude_lydo; ?></div>
                                <div class="r-box-text-detail"><?php echo $noidung_lydo; ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php  
                            if($i==4){ break; }
                            endwhile;
                        endif;
                        ?> 
                        <!-- <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-reason-2.svg" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title">Triển khai dễ dàng</div>
                                <div class="r-box-text-detail">Không cần cài đặt, chỉ cần 2 phút đăng ký là xuất được hóa đơn điện tử</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-reason-3.svg" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title">An toàn bảo mật cao</div>
                                <div class="r-box-text-detail">Trung tâm dữ liệu đạt tiêu chuẩn quốc tế Tier3, An ninh thông tin đạt ISO 27000, CSA STAR</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-reason-4.svg" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title">Tiết kiệm chi phí đầu tư</div>
                                <div class="r-box-text-detail">Không cần đầu tư cơ sở hạ tầng. Tiết kiệm 90% chi phí in ấn, vận chuyển, lưu trữ hóa đơn</div>
                            </div>
                            <div class="clear"></div>
                        </div> -->
                    </div>

                    <div class="new-col-md-10 home-reasons-right">
                        <?php
                            $j = 0;
                            if( have_rows('quản_ly_block_ly_do_chọn') ):
                                while ( have_rows('quản_ly_block_ly_do_chọn') ) : the_row(); $j++;

                            $icon_ly_do_chon = get_sub_field('icon_ly_do_chọn'); 
                            $tieude_lydo = get_sub_field('tieu_dề');
                            $noidung_lydo = get_sub_field('nội_dung'); 
                            //if($i>4){
                        ?>
                        <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php echo $icon_ly_do_chon; ?>" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title"><?php echo $tieude_lydo; ?></div>
                                <div class="r-box-text-detail"><?php echo $noidung_lydo; ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php  
                            //}
                            endwhile;
                        endif;
                        ?> 
                        <!-- <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-reason-6.svg" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title">Công nghệ hiện đại</div>
                                <div class="r-box-text-detail">Ứng dụng công nghệ Blockchain tiên tiến nhất hiện nay, chống giả mạo hóa đơn</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-reason-7.svg" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title">Thương hiệu uy tín</div>
                                <div class="r-box-text-detail">25 năm kinh nghiệm - Hơn 100 giải thưởng - Hơn 200.000 tổ chức và hàng triệu cá nhân</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="home-reasons-box">
                            <img class="cls-img-reasons img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/icon-reason-8.svg" alt="newinvoice" />
                            <div class="r-box-text">
                                <div class="r-box-text-title">Tư vấn tận tình</div>
                                <div class="r-box-text-detail">Tư vấn viên hỗ trợ tận tình 365 ngày/năm với nhiều kênh tư vấn: Forum, chatbot,...</div>
                            </div>
                            <div class="clear"></div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <!-- Báo giá -->
    <div class="home-price">
        <div class="container">
            <div class="row">
                <h2>
                    <div class="home-price-title"><?php echo get_field("tieu_dề_block_bao_gia", 11); ?></div>
                    <div class="home-price-name"><?php echo get_field("mo_tả_ngắn_block_bao_gia", 11); ?></div>
                </h2>
                <div class="new-col-md-24 no-padding">
                    <?php
                        $i = 0;
                        if( have_rows('quản_ly_goi_hoa_don', 11) ):
                            while ( have_rows('quản_ly_goi_hoa_don', 11) ) : the_row(); $i++;
                            if($i == 1){
                    ?>
                      <div class="new-col-md-6 new-col-md-offset-3 price-box-padding-first">
                          <div class="price-box-first">
                              <div class="box-first-title">
                                  <div class="first-mei-title">
                                      <?php echo get_sub_field('ma_goi_hoa_don'); ?>
                                  </div>
                                  <div class="first-price-title">
                                      <?php echo get_sub_field('gia_goi_hoa_don'); ?>
                                  </div>
                              </div>
                              <div class="box-first-content">
                                  <div class="content-box">
                                      <div class="content-number-invoice">
                                          <?php echo get_sub_field('số_hoa_don_trong_goi'); ?>
                                      </div>
                                  </div>
                                  <div class="content-number">
                                      <?php echo get_sub_field('gia_tiền_mỗi_hoa_don'); ?>
                                  </div>
                                  <div class="content-unit">
                                      VNĐ/Hóa đơn
                                  </div>
                                  <a target="_blank" href="<?php echo get_sub_field('link_mua_ngay'); ?>">
                                      <div class="price-btn-buy-now">
                                          Mua ngay
                                      </div>
                                  </a>
                              </div>
                          </div>
                      </div>
                    <?php } else if($i == 2){ ?>
                      <div class="new-col-md-6 price-box-padding-second">
                          <div class="price-box-second">
                              <div class="box-second-title">
                                  <div class="second-mei-title">
                                      <?php echo get_sub_field('ma_goi_hoa_don'); ?>
                                  </div>
                                  <div class="second-price-title">
                                      <?php echo get_sub_field('gia_goi_hoa_don'); ?>
                                  </div>
                              </div>
                              <div class="box-second-content">
                                  <div class="content-box">
                                      <div class="content-number-invoice">
                                          <?php echo get_sub_field('số_hoa_don_trong_goi'); ?>
                                      </div>
                                  </div>
                                  <div class="content-number">
                                      <?php echo get_sub_field('gia_tiền_mỗi_hoa_don'); ?>
                                  </div>
                                  <div class="content-unit">
                                      VNĐ/Hóa đơn
                                  </div>
                                  <a target="_blank" href="<?php echo get_field('link_mua_ngay'); ?>">
                                      <div class="price-btn-buy-now">
                                          Mua ngay
                                      </div>
                                  </a>
                              </div>
                          </div>
                      </div>
                    <?php } else if($i == 3){ ?>
                      <div class="new-col-md-6 price-box-padding-third">
                          <div class="price-box-third">
                              <div class="box-third-title">
                                  <div class="third-mei-title">
                                      <?php echo get_sub_field('ma_goi_hoa_don'); ?>
                                  </div>
                                  <div class="third-price-title">
                                      <?php echo get_sub_field('gia_goi_hoa_don'); ?>
                                  </div>
                              </div>
                              <div class="box-third-content">
                                  <div class="content-box">
                                      <div class="content-number-invoice">
                                          <?php echo get_sub_field('số_hoa_don_trong_goi'); ?>
                                      </div>
                                  </div>
                                  <div class="content-number">
                                      <?php echo get_sub_field('gia_tiền_mỗi_hoa_don'); ?>
                                  </div>
                                  <div class="content-unit">
                                      VNĐ/Hóa đơn
                                  </div>
                                  <a target="_blank" href="<?php echo get_sub_field('link_mua_ngay'); ?>">
                                      <div class="price-btn-buy-now">
                                          Mua ngay
                                      </div>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
                  <?php } ?>
                  <?php  
                      endwhile;
                  endif;
                  ?>
                  <div class="new-col-md-24">
                      <div class="price-btn-fee">
                          <span class="price-btn-fee-text"><?php echo get_field("phi_thue_bao_hang_nam", 11); ?></span>
                      </div>
                  </div>

            </div>
        </div>
    </div>
    <!---->
    <!-- Khách hàng nói về chúng tôi -->
    <div class="home-customers-about-us">
        <div class="container">
            <div class="row">
                <h2>
                    <div class="home-customers-about-us-title"><?php echo get_field("tieu_dề_block_kh_noi_về_chung_toi"); ?></div>
                    <div class="home-customers-about-us-name"><?php echo get_field("mo_tả_ngắn_block_kh_noi_về_chung_toi"); ?></div>
                </h2>
                <div class="new-col-md-24">
                    <div class="owl-carousel">
                        <?php
                            $i = 0;
                            if( have_rows('quản_ly_block_kh_noi_về_chung_toi') ):
                                while ( have_rows('quản_ly_block_kh_noi_về_chung_toi') ) : the_row(); $i++;

                            $ten_kh = get_sub_field('ten_kh'); 
                            $chucvu_kh = get_sub_field('chức_vụ_kh'); 
                            $avarta_kh = get_sub_field('avarta_kh'); 
                            $comment_kh = get_sub_field('comment_kh'); 
                        ?>
                        <div class="new-col-md-24 item active">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            <?php echo $comment_kh; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive" src="<?php echo $avarta_kh; ?>" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            <?php echo $ten_kh; ?>
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            <?php echo $chucvu_kh; ?>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <?php  
                            endwhile;
                        endif;
                        ?>  
                        <!-- <div class="new-col-md-24 item">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            MISA là công ty công nghệ hàng đầu nên tôi đánh giá rất cao về những giá trị mà phần mềm hóa đơn điện tử meInvoice.vn mang đến cho các doanh nghiệp.
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive" src="<?php bloginfo('template_url');?>/assets/images/hddt/CustomersAboutUs/icon-customer-about-us-2.png" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            Ông Nguyễn Văn Phụng
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            Vụ trưởng vụ quản lý thuế doanh nghiệp lớn tổng cục thuế
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-24 item">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            meInvoice.vn tích hợp trực tiếp trên phần mềm kế toán MISA giúp tiết kiệm được thời gian nhập liệu chứng từ.
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive" src="<?php bloginfo('template_url');?>/assets/images/hddt/CustomersAboutUs/icon-customer-about-us-3.png" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            Anh Võ Văn Tiến Danh
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            Kế toán trưởng - Công ty TNHH Shine Team Việt Nam
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-24 item">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            MISA là một trong các doanh nghiệp Việt Nam đạt chứng nhận an toàn thông tin ISO 27001. Tôi tin rằng dịch vụ điện toán đám mây sẽ đáp ứng tốt.
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/CustomersAboutUs/icon-customer-about-us-1.png" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            Ông Nguyễn Minh Hồng
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            Thứ trưởng - Bộ thông tin và truyền thông
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-24 item">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            meInvoice.vn giúp tôi chuyển hóa đơn cho khách hàng qua email nhanh chóng và thuận tiện, tiết kiệm thời gian, chi phí, và không lo bị thất lạc.
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/CustomersAboutUs/icon-customer-about-us-2.png" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            Chị Hòa
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            Kế toán trưởng - Công ty TNHH MAHA Việt Nam
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-24 item">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            Phần mềm hóa đơn điện tử meInvoice.vn của MISA đáp ứng được toàn bộ yêu cầu của chúng tôi. Tôi đặc biệt ấn tượng về sự tận tình, chu đáo và chuyên nghiệp của các bạn.
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/CustomersAboutUs/icon-customer-about-us-3.png" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            Chị Nguyễn Thùy Dung
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            Kế toán trưởng - Công ty TNHH đầu tư và thương mại AAA Việt Nam
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="new-col-md-24 item">
                            <div class="content-item">
                                <div class="home-customers-about-us-comment">
                                    <div class="comment-padding">
                                        <div class="comment-content">
                                            Với meInvoice.vn, chỉ click chuột là "đầu cầu" bên kia đã nhận được hóa đơn mà không cần nhiều bước thực hiện, đặc biệt tiết kiệm thêm khoản chi phí vận chuyển nữa.
                                        </div>
                                    </div>
                                </div>
                                <div class="home-customers-about-us-info">
                                    <div class="home-customers-about-us-img">
                                        <img class="img-responsive img-lazy-load" src="<?php bloginfo('template_url');?>/assets/images/hddt/CustomersAboutUs/icon-customer-about-us-3.png" alt="newinvoice" />
                                    </div>
                                    <div class="home-customers-about-us-detail">
                                        <div class="home-customers-about-us-customer-name">
                                            Chị Thu Vân
                                        </div>
                                        <div class="home-customers-about-us-department">
                                            Kế toán - Công ty TNHH đầu tư và phát triển Phương Việt
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
</div>
<script type="text/javascript">
    $('.home-news-about-us .owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            pagination: true,
             dots : true,
            autoplay:false,
            autoplayTimeout:2000,

            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
    $('.home-news-list.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            pagination: true,
             dots : true,
            autoplay:false,
            autoplayTimeout:2000,

            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:2
                }
            }
        });
    $('.home-customers-about-us .owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            pagination: true,
            dots : true,
            autoplay:false,
            autoplayTimeout:2000,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        })
</script>
<!--=== End PageMainContent ===-->