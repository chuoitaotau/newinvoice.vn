<?php
/**
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hanko
 */
global $data;
?>
<!-- Our Clients -->
<div class="flexslider home clients" id="clients-flexslider">
    <div class="headline"><a href="khach-hang.html" target="_blank"><h2>Khách hàng</h2><!-- <span class="view">Xem tất cả <i class="fa fa-arrow-circle-right"></i></span> --></a></div>
    <div class="flex-viewport">
        <ul class="slides">
            <?php
                $i = 0;
                if( have_rows('quản_ly_khach_hang_dối_tac', 2) ):
                    while ( have_rows('quản_ly_khach_hang_dối_tac', 2) ) : the_row(); $i++;

                $logo_doi_tac = get_sub_field('logo_dối_tac'); 
                $link_doi_tac = get_sub_field('link_dối_tac'); 
            ?>
            <li>
                <a href="<?php echo $link_doi_tac; ?>">
                    <img class="lazy" alt="Doi-tac-cua-chung-toi" src="<?php echo $logo_doi_tac; ?>">
                </a>
            </li>
            <?php  
                endwhile;
            endif;
            ?>  
        </ul>
    </div>
</div><!--/flexslider-->
<!-- //End Our Clients -->